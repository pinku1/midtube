TopAlbums = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    getMeteorData() {
        var handle = Meteor.subscribe('topAlbums', Session.get('defaultCategory'));

        return {
            loading: ! handle.ready(),
            albums: Album.find({}, {sort: {baseScore: -1}, limit: 10}).fetch()
        }
    },

    renderTopAlbums() {
        return this.data.albums.map((album) => {
            return <AlbumItemList
                key={album._id}
                album={album} />
        });
    },
    
    render() {
        return (
            <div className="item background">
                <h3 className="ui dividing header">Weekly Top Albums</h3>
                <Loading active={this.data.loading} />
                <div className="ui items">
                    {this.renderTopAlbums()}
                </div>
            </div>
        )
    }
});
