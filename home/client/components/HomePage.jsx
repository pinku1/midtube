HomePage = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData, SmoothScrollMixin],

    componentDidMount: function() {
        SEO.set({
            title: 'MidTube.com | Listen and Download Music',
            description: 'At MidTube.com You can listen and download unlimited music',
            meta: {
              //'property="og:image"': 'http://locationofimage.com/image.png'
            }
          });
    },

    getMeteorData() {
        var handle = Meteor.subscribe('featuredAlbums', Session.get('defaultCategory'));

        return {
            loading: ! handle.ready(),
            featuredAlbums: Album.find({featured: true}, {sort: {created: -1}, limit: 12}).fetch(),
            isMobile: Session.get('isMobile')
        }
    },

    renderFeaturedAlbums() {
        return this.data.featuredAlbums.map((album) => {
            return <AlbumItem
                key={album._id}
                album={album} />
        });
    },

    render() {
        return <div>
            <Parallax bgImage="/images/blur.jpg" strength={300} blur={1}>
                <div className="ui grid container">
                    <h2 className="ui header artist" style={{'width': '100%'}}>
                        <div className="content" style={{'color':'#fff'}}>
                            Discover and share amazing music. Listen and Download.
                        </div>
                    </h2>
                </div>
            </Parallax>
            <div className="main">
                <PageHeader
                    title="Featured albums"
                    moreText="All albums"
                    moreLink={FlowRouter.path('albums')} />
                <Loading active={this.data.loading} />
                {this.data.featuredAlbums.length > 0 ?
                    <div className="ui doubling centered six column grid container">
                        {this.renderFeaturedAlbums()}
                    </div>
                    :
                    <div className="ui message grid container">
                        <div className="header">
                            No featured albums found
                        </div>
                    </div>
                }
                <div className="ui section divider"></div>
                <div className="ui stackable column grid container">
                    <div className="eleven wide column">
                        <PageHeader title="Featured artists" />
                        <HomeArtists />
                        { !this.data.isMobile ?
                            <HomeAdverts />
                        : '' }
                        { !this.data.isMobile ?
                            <HomeNews />
                            : '' }
                    </div>
                    { !this.data.isMobile ?
                        <div className="five wide column" style={{'backgroundColor': '#fff'}}>
                            <TopAlbums />
                        </div>
                    : '' }
                </div>
            </div>
        </div>
    }
});
