HomeAdverts = React.createClass({
    render() {
        return <div className="ui grid">
            <div className="sixteen wide column">
                <div className="ui leaderboard ad" data-text="Leaderboard" style={{'margin': '0 auto', 'width': '100%'}}>
                    <a href="http://www.sanjhafm.com" target="_blank">
                        <img src="https://s3-us-west-2.amazonaws.com/sanjha-metro/banners/sanjha-fm.jpg" style={{'width': '100%'}} />
                    </a>
                </div>
            </div>
        </div>
    }
});
