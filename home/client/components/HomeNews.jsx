HomeNews = React.createClass({
    getInitialState: function() {
        return {
            posts: [],
            loading: true
        };
    },

    componentDidMount() {
        var self = this;
        ghost.init({
            clientId: "ghost-frontend",
            clientSecret: "87866261542c"
        });

        function onSuccess(data) {
            self.setState({
               posts: data.posts,
               loading: false
            });
        }

        jQuery(document).ready(function () {
            $.get(
                ghost.url.api('posts', {limit: 6})
            ).done(onSuccess);
        });
    },

    renderNews() {
        return this.state.posts.map(function (post) {
            return (
                <div className="item">
                    <div className="ui tiny image">
                        {_.isNull(post.image) ?
                        <img src="/images/midtube-logo.jpg" />
                        : 
                        <img src={'https://news.midtube.com' + post.image} />
                        }
                    </div>
                    <div className="middle aligned content">
                        <a className="header" href={'https://news.midtube.com' + post.url}>
                            {post.title}
                        </a>
                        <div class="meta">
                          <span class="cinema">
                            {humanDate(post.published_at)}
                          </span>
                        </div>
                    </div>
                </div>
            )
        });
    },

    render() {
        return (
            <div className="item background" style={{'marginTop': '25px'}}>
                <h3 className="ui dividing header">Latest News</h3>
                <div className="ui items">
                    {this.renderNews()}
                </div>
            </div>
        )
    }
});
