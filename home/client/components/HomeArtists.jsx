HomeArtists = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    getMeteorData() {
        var handle = Meteor.subscribe('featuredArtists');

        var userFind = {featured: true};
        //don't show current user fix
        if (!_.isNull(Meteor.userId())) {
            userFind.userId = {'$ne': Meteor.userId()};
        }
        return {
            loading: ! handle.ready(),
            artists: Artist.find(userFind, {sort: {createdAt: -1}, limit: 4}).fetch()
        }
    },

    renderArtists() {
        return this.data.artists.map((artist) => {
            return <ArtistItem
                key={artist._id}
                artist={artist} />
        });
    },

    render() {
        return <div className="ui doubling centered four column grid container">
            <Loading active={this.data.loading} />
            {this.renderArtists()}
        </div>
    }
});
