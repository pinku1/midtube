ContactView = React.createClass ({
    getInitialState() {
        return {
            fullName: '',
            email: '',
            subject: '',
            message: ''
        };
    },

    componentDidMount() {
        $('.ui.form')
            .form({
                fields: {
                    fullName: {
                        identifier: 'fullName',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Full Name"'
                            }
                        ]
                    },
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Email"'
                            }
                        ]
                    },
                    subject: {
                        identifier: 'subject',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Subject"'
                            }
                        ]
                    },
                    message: {
                        identifier: 'message',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Message"'
                            }
                        ]
                    }
                }
            });
    },

    handleChange(e) {
        this.setState({
            fullName: ReactDOM.findDOMNode(this.refs.fullName).value,
            email: ReactDOM.findDOMNode(this.refs.email).value,
            subject: ReactDOM.findDOMNode(this.refs.subject).value,
            message: ReactDOM.findDOMNode(this.refs.message).value
        });
    },

    submitForm(ev) {
        var data = {
            fullName: ReactDOM.findDOMNode(this.refs.fullName).value.trim(),
            email: ReactDOM.findDOMNode(this.refs.email).value.trim(),
            subject: ReactDOM.findDOMNode(this.refs.subject).value.trim(),
            message: ReactDOM.findDOMNode(this.refs.message).value.trim()
        };

        Meteor.call('submitContact', data, function (err, res) {
            FlashMessages.sendSuccess('Thank you for contacting!');
        });
    },

    render() {
        return <div className="main">
            <PageHeader title="Contact us" />
            <div className="ui grid container page main">
                <form className="ui form" onSubmit={this.submitForm} style={{'width': '50%'}}>
                    <div className="field">
                        <label>Full Name</label>
                        <input
                            name="fullName"
                            ref="fullName"
                            onChange={this.handleChange}
                            value={this.state.fullName}
                            placeholder="Your full name"/>
                    </div>
                    <div className="field">
                        <label>Email</label>
                        <input
                            type="email"
                            name="email"
                            ref="email"
                            onChange={this.handleChange}
                            value={this.state.email}
                            placeholder="Your email"/>
                    </div>
                    <div className="field">
                        <label>Subject</label>
                        <input
                            name="subject"
                            ref="subject"
                            onChange={this.handleChange}
                            value={this.state.subject}
                            placeholder="Message subject"/>
                    </div>
                    <div className="field">
                        <label>Message</label>
                        <textarea
                            rows="2"
                            name="message"
                            ref="message"
                            onChange={this.handleChange}
                            value={this.state.message}
                            placeholder="Message"></textarea>
                    </div>
                    <button className="ui labeled icon blue button" type="submit">
                        <i className="send icon"></i>
                        Submit
                    </button>
                    <div className="ui error message"></div>
                </form>
            </div>
        </div>
    }
});
