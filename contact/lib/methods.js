Meteor.methods({
    'submitContact': function (doc) {
        if (!_.isNull(this.userId)) {
            ///if user is logged in, log it's id
            doc = _.extend(doc, {
                sentByUserId: this.userId
            });
        }

        var contact = new Contact();
        contact.set(doc);
        contact.validate();

        if (contact.hasValidationErrors()) {
            contact.throwValidationException();
        } else {
            //todo send email notification to system admin

            contact.save();
            return contact;
        }
    }
});
