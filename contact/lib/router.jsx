FlowRouter.route('/contact', {
    name: 'contactView',
    action: function() {
        ReactLayout.render(MainLayout, {
            content: <ContactView />
        });
    }
});
