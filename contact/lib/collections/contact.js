var requiredString = Validators.and([
    Validators.required(),
    Validators.string(),
    Validators.minLength(3),
    Validators.maxLength(40)
]);

Contacts = new Mongo.Collection('contacts');

Contact = Astro.Class({
    name: 'Contact',
    collection: Contacts,
    fields: {
        fullName: {
            type: 'string',
            validator: requiredString
        },
        email: {
            type: 'string',
            validator: requiredString
        },
        subject: {
            type: 'string'
        },
        message: 'string',
        sentByUserId: 'string'
    },
    behaviors: {
        timestamp: {
            hasCreatedField: true,
            createdFieldName: 'createdAt',
            hasUpdatedField: true,
            updatedFieldName: 'updatedAt'
        }
    }
});
