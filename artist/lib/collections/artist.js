var requiredString = Validators.and([
    Validators.required(),
    Validators.string(),
    Validators.minLength(3),
    Validators.maxLength(40)
]);
var requiredDate = Validators.and([
    Validators.required(),
    Validators.date()
]);

Artists = new Mongo.Collection('artists');

Artist = Astro.Class({
    name: 'Artist',
    collection: Artists,
    fields: {
        //listener, singer, record-label
        title: {
            type: 'string',
            validator: requiredString,
            default: function() {
                return 'listener';
            }
        },
        fullName: {
            type: 'string',
            validator: requiredString
        },
        slug: {
            type: 'string',
            validator: Validators.unique(),
            default: function() {
                return Random.id(8);
            }
        },
        biography: 'string',
        social: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'social',
                fields: {
                    twitter: 'string',
                    facebook: 'string',
                    instagram: 'string'
                }
            }
        },
        contact: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'contact',
                fields: {
                    mobile: 'string',
                    address: 'string',
                    website: 'string'
                }
            }
        },
        featured: {
            type: 'boolean',
            default: false
        },
        userId: 'string',
        profilePicture: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'profilePicture',
                fields: {
                    name: 'string',
                    pathOriginal: 'string',
                    path: 'string',
                    size: {
                        type: 'number',
                        validator: Validators.number(),
                        default: 0
                    },
                    type: 'string'
                }
            }
        },
        baseScore: {
            type: 'number',
            default: 0
        },
        subscribers: {
            type: 'number',
            default: 0
        },
        subscribedBy: {
            type: 'array',
            default: function() {
                return [];
            }
        }
    },
    behaviors: {
        slug: {
            fieldName: 'fullName',
            methodName: null,
            slugFieldName: 'slug',
            canUpdate: true,
            unique: true,
            separator: '-'
        },
        timestamp: {
            hasCreatedField: true,
            createdFieldName: 'createdAt',
            hasUpdatedField: true,
            updatedFieldName: 'updatedAt'
        }
    }
});
