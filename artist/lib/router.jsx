FlowRouter.route('/profile/update', {
    name: 'profileUpdate',
    subscriptions: function(params) {
        this.register('userProfile', Meteor.subscribe('userProfile', null));
    },
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <ProfileUpdate />
        });
    }
});

FlowRouter.route('/profile/music', {
    name: 'profileMusic',
    subscriptions: function(params) {
        this.register('userAlbums', Meteor.subscribe('userAlbums'));
    },
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <ProfileMusic />
        });
    }
});

FlowRouter.route('/profile/:slug', {
    name: 'profileView',
    subscriptions: function(params) {
        this.register('userProfile', Meteor.subscribe('userProfile', params.slug));
    },
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <ArtistView slug={params.slug} />
        });
    }
});
