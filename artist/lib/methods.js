Meteor.methods({
    'createArtist': function () {
        var user = Meteor.user();
        //check if profile exists, if not create one
        if (_.isUndefined(Artist.findOne({userId: user._id}))) {
            var doc = {userId: this.userId};

            //get data from social networks if possible
            if (!_.isUndefined(user.profile) && !_.isUndefined(user.profile.name)) {
                doc.fullName = user.profile.name;
            }

            return Artist.insert(doc);
        } else {
            return null;
        }
    },

    'updateArtist': function (doc) {
        if (_.isNull(this.userId)) {
            //if not logged in
            throw new Meteor.Error('not-logged-in', 'Please login to continue.');
        }

        //make sure owner is current logged in user
        var artist = Artist.findOne({userId: this.userId});

        //slugify slug
        if (!_.isUndefined(doc.slug)) {
            doc.slug = s.slugify(doc.slug);
        }

        //clean and trim
        if (!_.isUndefined(doc.fullName)) {
            doc.fullName = clean(doc.fullName);
        }
        if (!_.isUndefined(doc.biography)) {
            doc.biography = s.stripTags(clean(doc.biography));
        }

        artist.set(doc);

        artist.validate();

        if (artist.hasValidationErrors()) {
            artist.throwValidationException();
        } else {
            artist.save();
            return artist;
        }
    },

    'updateUserEmail': function (doc) {
        return Meteor.users.update({_id: this.userId}, {$set: {
            'emails.0.address': doc.email,
            'emails.0.verified' : false
        }})
    },

    'updateUserPayment': function (doc) {
        return Meteor.users.update({_id: this.userId}, {$set: {
            'payment.paypalId': doc.paypalId,
            'payment.bankCheckName' : doc.bankCheckName
        }})
    }
});
