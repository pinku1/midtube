ArtistActionArea = React.createClass({
	// This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        artist: React.PropTypes.object.isRequired
    },

    getMeteorData() {
        return {
          currentUserId: Meteor.userId(),
          currentUser: Meteor.user()
        };
    },

    profileUpdateUrl() {
        return FlowRouter.path('/profile/update');
    },

    isOwner() {
        return Meteor.userId() === this.props.artist.userId;
    },

    getSocial(name) {
        if (!_.isNull(this.props.artist.social[name]) && this.props.artist.social[name].length > 0) {
            return this.props.artist.social[name];
        }

        return false;
    },

    doSubscribe() {
        this.handleVoteClick('subscribeArtist');
    },

    hasSubscribed() {
        if (_.isUndefined(this.props.artist.subscribedBy) || this.props.artist.subscribedBy.length <= 0) {
            return false;
        }

        return _.indexOf(this.props.artist.subscribedBy, this.data.currentUserId) != '-1';
    },

    handleVoteClick(meteorMethodName) {
        //e.preventDefault();
        if (_.isNull(this.data.currentUser)) {
            console.log('login first!');
            $('#loginModal')
                .modal({detachable: false})
                .modal('show');
            //FlowRouter.go('/login');
            //flashMessage('Please login first', 'info');
        } else {
            Meteor.call(meteorMethodName, this.props.artist, function (error, result) {
                //console.log(error);
                //console.log(result);
                FlashMessages.send('Success!');
            });
        }
    },

    getShareTitle() {
        return s.capitalize(this.props.artist.fullName);
    },
    
	render() {
		return <div className="ui grid container" style={{'margin': '10px 0'}}>
            <div className="ui compact stackable menu action" style={{'width': '100%'}}>
                {this.isOwner() ?
                    <a className="item" href={this.profileUpdateUrl()}>
                        <i className="setting icon"></i>
                        Update Profile
                    </a>
                    : '' }
                <div className="item">
                    <button className="ui labeled button" tabIndex="0" onClick={this.doSubscribe}>
                        <div className="ui red button">
                            <i className={this.hasSubscribed() ? 'icon checkmark box' : 'icon checkmark'}></i> {this.hasSubscribed() ? 'Subscribed' : 'Subscribe'}
                        </div>
                        <a className="ui basic red left pointing label" style={{'paddingBottom': '9px'}}>
                            {this.props.artist.subscribers || 0}
                        </a>
                    </button>
                </div>
                <div className="item">
                    <SocialSharePopUp title={this.getShareTitle()} />
                </div>
                <div className="right menu">
                    <div className="item">
                        {!_.isUndefined(this.props.artist.contact) && !_.isNull(this.props.artist.contact.website) && this.props.artist.contact.website ?
                            <a className="ui circular icon button" href={'http://' + this.props.artist.contact.website} target="_blank" style={{'margin': '-0.5em 0.2em'}}>
                                <i className="browser icon"></i>
                            </a>
                            : '' }
                        {this.getSocial('facebook') ?
                            <a className="ui circular facebook icon button" href={this.getSocial('facebook')} target="_blank" style={{'margin': '-0.5em 0.2em'}}>
                                <i className="facebook icon"></i>
                            </a>
                        : '' }
                        {this.getSocial('twitter') ?
                            <a className="ui circular twitter icon button" href={this.getSocial('twitter')} target="_blank" style={{'margin': '-0.5em 0.2em'}}>
                                <i className="twitter icon"></i>
                            </a>
                        : '' }
                        {this.getSocial('instagram') ?
                            <a className="ui circular instagram icon button" href={this.getSocial('instagram')} target="_blank" style={{'margin': '-0.5em 0.2em'}}>
                                <i className="instagram icon"></i>
                            </a>
                        : '' }
                    </div>
                </div>
            </div>
        </div>
	}
});
