ArtistView = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        slug: React.PropTypes.string.isRequired
    },

    componentWillMount: function() {
        if (!_.isUndefined(this.data.artist)) {
            SEO.set({
                title: this.data.artist.fullName + ' - Listen and download music on MidTube.com',
                description: 'Listen and download music from ' + this.data.artist.fullName + ' plus share and discover new music!',
                meta: {
                    'property="og:image"': !_.isNull(this.data.artist.profilePicture.path) ? this.data.artist.profilePicture.path.replace(/ /g, '%20') : 'http://www.midtube.com/images/avatar.jpg'
                }
            });
        }
    },

    getMeteorData() {
        var artist = Artist.findOne({slug: this.props.slug});
        if (_.isUndefined(artist)) {
            //todo artist not found
            //FlowRouter.go("/not-found");
        }

        return {
            artist: artist,
            isOwner: !_.isUndefined(artist) && Meteor.userId() === artist.userId
        }
    },

    render() {
        return (
            <div>
                {!_.isUndefined(this.data.artist) ?
                    <div>
                        <Parallax bgImage="/images/blur.jpg" strength={300} blur={1}>
                            <ArtistViewHead artist={this.data.artist}/>
                        </Parallax>

                        <ArtistActionArea artist={this.data.artist} />

                        <ArtistAlbums artist={this.data.artist} isOwner={this.data.isOwner} />

                        <ArtistVideos artist={this.data.artist} isOwner={this.data.isOwner} />

                        <div className="ui section divider"></div>

                        <FavoriteAlbums artist={this.data.artist} />
                    </div>
                    : 'loading...'}
                <DoReload currentPage={this.props.slug} />
            </div>
        )
    }
});
