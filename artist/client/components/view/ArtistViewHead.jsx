ArtistViewHead = React.createClass({
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        artist: React.PropTypes.object.isRequired
    },

    componentDidMount() {
        //color thief back overlay
        //if (!_.isNull(this.props.artist.profilePicture.path)) {
        //    var image = new Image();
        //    image.crossOrigin = "anonymous";
        //    image.src = this.props.artist.profilePicture.path;
        //    setTimeout(function () {
        //        var colorThief = new ColorThief.colorRob();
        //        var res = colorThief.getColor(image);
        //
        //        if (!_.isUndefined(res[0])) {
        //            $(".react-parallax-content").css("background-color", "rgba(" + res[0] + "," + res[1] + "," + res[2] + ",0.2)");
        //        }
        //    }, 1200);
        //}
    },

    getMeteorData() {
        return {
            isMobile: Session.get('isMobile')
        }
    },

    getLocation() {
        if (!_.isNull(this.props.artist.contact.address) && this.props.artist.contact.address.length > 0) {
            return this.props.artist.contact.address;
        }

        return false;
    },

    render() {
        var contactsStyle = {'color':'#fff', 'textAlign': 'left', 'fontSize': '1.25rem'};
        if (this.data.isMobile) {
            contactsStyle['width'] = '100%';
        } else {
            contactsStyle['width'] = '36%';
            contactsStyle['float'] = 'right';
        }

        return <div className="ui grid container">
            <h2 className="ui header artist" style={{'width': '100%'}}>
                <ArtistImage artist={this.props.artist} />
                <div className="content" style={{'color':'#fff'}}>
                    {titleize(this.props.artist.fullName)}<br/>
                    <small><ArtistTitle title={this.props.artist.title} /></small>
                    {this.getLocation() ?
                        <div className="sub header">
                            <i className="marker icon"></i>{titleize(this.getLocation())}
                        </div>
                    : '' }
                </div>
                <div className="contacts" style={contactsStyle}>
                    { !_.isUndefined(this.props.artist.biography) && !_.isNull(this.props.artist.biography) && this.props.artist.biography.length > 0 ?
                        <p dangerouslySetInnerHTML={{__html: this.props.artist.biography.replace(/\r?\n/g, '<br />')}} />
                    : '' }
                    { !_.isUndefined(this.props.artist.contact) && !_.isNull(this.props.artist.contact.mobile) && this.props.artist.contact.mobile ?
                        <p>Tel: <a href={'tel:' + this.props.artist.contact.mobile} style={{'color': '#fff'}}>{this.props.artist.contact.mobile}</a></p>
                    : '' }
                </div>
            </h2>
        </div>
    }
});
