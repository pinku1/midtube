ArtistAlbums = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        artist: React.PropTypes.object.isRequired,
        isOwner: React.PropTypes.bool
    },

    getMeteorData() {
        var subscription = Meteor.subscribe('allArtistAlbums', this.props.artist._id, this.props.artist.userId);

        var where = {};
        if (!_.isNull(this.props.artist.userId)) {
            where = {'$or': [{ownerId: this.props.artist.userId}, {artists: {'$elemMatch': {_id: this.props.artist._id}}}]};
        } else {
            where = {artists: {'$elemMatch': {_id: this.props.artist._id}}};
        }
        return {
            loading: ! subscription.ready(),
            albums: Album.find(where, {sort: {createdAt: -1}, 'limit': 60}).fetch()
        }
    },

    renderAlbums() {
        return this.data.albums.map((album) => {
            return <AlbumItem
                key={album._id}
                album={album} />
        });
    },

    render() {
        return (
            <div style={{'backgroundColor': '#fff', 'padding': '40px 0 2px 0', 'margin': '40px 0'}}>
                <Loading active={this.data.loading} />
                <div className="ui grid container">
                    <h2 className="ui header">
                        <ArtistTitle title={this.props.artist.title} />'s Albums
                    </h2>
                    {this.props.isOwner ?
                        <a className="circular ui icon button right floated green" style={{'height': '34px'}} href="/album/add">
                            <i className="icon add"></i>
                        </a>
                        : '' }
                </div>
                <div className="ui doubling centered six column grid main container">
                    { this.data.albums.length > 0 ?
                        this.renderAlbums()
                        :
                        <div className="ui message">
                            <div className="header">
                                No albums for artist found
                            </div>
                        </div>
                    }
                </div>
            </div>
        )
    }
});
