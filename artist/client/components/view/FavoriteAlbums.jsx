FavoriteAlbums = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        artist: React.PropTypes.object.isRequired
    },

    getMeteorData() {
        var subscription = Meteor.subscribe('favoriteAlbums', this.props.artist.userId);

        var favAlbums = [];
        if (!_.isNull(this.props.artist.userId) && !_.isUndefined(this.props.artist.userId)) {
            favAlbums = Album.find({favoriteBy: this.props.artist.userId}, {sort: {createdAt: -1}, limit: 60}).fetch();
        }
        return {
            loading: ! subscription.ready(),
            favAlbums: favAlbums
        }
    },

    renderAlbums() {
        return this.data.favAlbums.map((album) => {
            return <AlbumItem
                key={album._id}
                album={album} />
        });
    },

    render() {
        return <div>
            <Loading active={this.data.loading} />
            { this.data.favAlbums.length > 0 ?
                <div>
                    <PageHeader title="Favorite music"/>
                    <div className="ui doubling centered six column grid main container">
                        { this.renderAlbums() }
                    </div>
                </div>
            : '' }
        </div>
    }
});
