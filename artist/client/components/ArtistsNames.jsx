ArtistsNames = React.createClass({
    propTypes: {
        artists: React.PropTypes.array.isRequired,
        truncate: React.PropTypes.bool
    },

    getPermalink(artist) {
        //console.log(artist);
        if (!_.isNull(artist.slug)) {
            return FlowRouter.path('/profile/:slug', {slug: artist.slug});
        } else {
            return FlowRouter.path('/search/:query', {query: artist.fullName});
        }
    },

    renderArtist() {
        if (_.isUndefined(this.props.artists)) {
            return '';
        }

        var truncateName = _.isUndefined(this.props.truncate) ? false : this.props.truncate;

        return this.props.artists.map((artist) => {
            return <a
                key={artist._id}
                className="artistName"
                href={this.getPermalink(artist)}>
                {truncateName ?
                    truncate(titleize(artist.fullName))
                    :
                    titleize(artist.fullName)
                }
            </a>
        });
    },

    render() {
        return <span>
            {this.renderArtist()}
        </span>
    }
});
