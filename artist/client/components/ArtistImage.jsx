ArtistImage = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        artist: React.PropTypes.object.isRequired
    },

    getPicture() {
        if (!_.isNull(this.props.artist.profilePicture.path)) {
            return this.props.artist.profilePicture.path.replace(/ /g, '%20');
        } else {
            return '/images/avatar.png';
        }
    },

    render() {
        return <img
            className="ui small bordered image"
            src={this.getPicture()}
            title={this.props.artist.title}
            style={{'width': '200px', 'height': 'auto'}} />
    }
});
