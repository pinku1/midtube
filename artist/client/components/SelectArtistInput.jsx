/**
 * TODO!!!
 */
SelectArtistInput = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        artist: React.PropTypes.object.isRequired,
        handleChange: React.PropTypes.func
    },

    getInitialState() {
        return {
            artistIds: this.props.artist.fullName || '',
            artist: this.props.artist || {}
        };
    },

    getMeteorData() {
        Meteor.subscribe('getArtistByTitle', 'singer');

        return {
            singers: Artist.find({title: 'singer'}, {sort: {title: 1}}).fetch()
        };
    },

    componentDidMount() {
        $('.artistSelect').dropdown();
    },

    renderArtist() {
        return this.data.singers.map((singer) => {
            return <option key={singer._id} value={singer._id}>{singer.fullName}</option>
        });
    },

    render() {
        return <select
            name="categoryId"
            ref="categoryId"
            className="ui fluid search dropdown artistSelect"
            multiple="true"
            value={this.state.artistIds}
            onChange={this.handleChange}>
            <option value="">Select Artists</option>
            {this.renderArtist()}
        </select>
    }
});
