ArtistTitle = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired
    },

    render() {
        return <span>
            {titleize(this.props.title.replace(/_/g, ' '))}
        </span>
    }
});
