ArtistItem = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        artist: React.PropTypes.object.isRequired
    },

    getArtistUrl() {
        return FlowRouter.path('/profile/:slug', {slug: this.props.artist.slug});
    },

    render() {
        return  <div className="column">
            <div className="ui card">
                <a className="image" href={this.getArtistUrl()}>
                    <ArtistImage artist={this.props.artist} />
                </a>
                <div className="content">
                    <a className="header" href={this.getArtistUrl()}>
                        {truncate(titleize(this.props.artist.fullName))}
                    </a>
                    <div className="meta">
                        <a>{truncate(titleize(this.props.artist.title))}</a>
                    </div>
                </div>
            </div>
        </div>
    }
});
