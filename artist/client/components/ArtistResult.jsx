ArtistResultChild = React.createClass({
    propTypes: {
        artists: React.PropTypes.array.isRequired,
        totalCount: React.PropTypes.number.isRequired
    },

    getInitialState() {
        return {
            currentCount: AppSettings.limit,
            hasMore: false
        };
    },

    componentDidMount() {
        this.setState({
            hasMore: this.state.currentCount < this.props.totalCount
        });
    },

    componentWillReceiveProps(nextProps) {
        this.setState({
            hasMore: this.state.currentCount < nextProps.totalCount
        });
    },

    loadMore() {
        var increment = AppSettings.limit;
        var limit = increment * 2;
        if (!_.isUndefined(FlowRouter.getQueryParam("artist-limit"))) {
            limit = parseInt(FlowRouter.getQueryParam("artist-limit")) + increment;
        }

        this.setState({
            currentCount: limit
        });
        FlowRouter.setQueryParams({'artist-limit': limit});
    },

    renderArtists() {
        return this.props.artists.map((artist) => {
            return <ArtistItem
                key={artist._id}
                artist={artist} />
        });
    },

    render() {
        return <div>
            {this.props.totalCount > 0 ?
                <div className="ui doubling centered six column grid">
                    {this.renderArtists()}
                </div>
                :
                <div className="ui message">
                    <div className="header">
                        No artists found.
                    </div>
                </div>
            }
            <div className="ui grid">
                {this.state.hasMore ?
                    <button className="ui button fluid" onClick={this.loadMore}>
                        Load more
                    </button>
                    : '' }
            </div>
        </div>
    }
});

ArtistResult = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    getInitialState() {
        return {
            currentCount: AppSettings.limit,
            totalCount: Counts.get('total-artists-count')
        };
    },

    getMeteorData() {
        var userFind = {};
        //don't show current user fix
        if (!_.isNull(Meteor.userId())) {
            userFind.userId = {'$ne': Meteor.userId()};
        }

        return {
            artists: Artist.find(userFind, {sort: {createdAt: -1, baseScore: -1}}).fetch(),
            totalCount: Counts.get('total-artists-count')
        }
    },

	render() {
		return <ArtistResultChild
            artists={this.data.artists}
            totalCount={this.data.totalCount} />
	}
});
