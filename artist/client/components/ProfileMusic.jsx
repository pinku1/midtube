ProfileMusic = React.createClass({
    mixins: [ReactMeteorData],

    getMeteorData() {
        return {
            albums: Album.find({ownerId: Meteor.userId()}, {sort: {createdAt: -1}}).fetch()
        }
    },

    deleteItem(albumId, ev) {
        Meteor.call('removeAlbum', albumId);
    },

    renderAlbums() {
        return this.data.albums.map((album) => {
            return <tr key={album._id}  className={classNames({'negative': !album.isActive})}>
                <td className="collapsing">
                    <AlbumActiveToggle album={album} />
                </td>
                <td>{album.title}</td>
                <td><ArtistsNames artists={album.artists} /></td>
                <td>{humanDate(album.releaseDate)}</td>
                <td>0</td>
                <td>0</td>
                <td>
                    <a className="mini ui primary button" href={'/album/' + album._id}>
                        View
                    </a>
                    <a className="mini ui blue button" href={'/album/update/info/' + album._id}>
                        Edit
                    </a>
                    <a className="mini ui red button" onClick={this.deleteItem.bind(this, album._id)}>
                        Delete
                    </a>
                </td>
            </tr>
        });
    },

    render() {
        return (
            <div className="main">
                <PageHeader title="Manage Music" />
                <div className="ui grid container main">
                    {this.data.albums.length > 0 ?
                        <table className="ui compact celled definition selectable table">
                            <thead>
                            <tr>
                                <th>Is Public</th>
                                <th>Title</th>
                                <th>Artist</th>
                                <th>Release Date</th>
                                <th>Total Sales</th>
                                <th>Amount Earned ($)</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.renderAlbums()}
                            </tbody>
                            <tfoot className="full-width">
                            <tr>
                                <th></th>
                                <th colSpan="6">
                                    <div className="ui right floated small primary labeled icon button">
                                        <i className="plus icon"></i> Add Album
                                    </div>
                                </th>
                            </tr>
                            </tfoot>
                        </table>
                        :
                        <div className="ui message grid container">
                            <div className="header">
                                No albums found, <a href="/album/add">click here to add one</a>
                            </div>
                        </div>
                    }
                </div>
            </div>
        )
    }
});
