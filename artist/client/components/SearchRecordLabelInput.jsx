SearchRecordLabelInput = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        recordLabel: React.PropTypes.object.isRequired,
        handleChange: React.PropTypes.func
    },

    getInitialState() {
        return {
            recordLabelTitle: this.props.recordLabel.title || '',
            recordLabel: this.props.recordLabel || {}
        };
    },

    getMeteorData() {
        Meteor.subscribe('getArtistByTitle', 'record-label');

        return {
            recordLabels: Artist.find({title: 'record-label'}, {sort: {title: 1}}).fetch()
        };
    },

    updateRecordLabel() {
        var root = this;
        this.setState({
            recordLabelTitle: ReactDOM.findDOMNode(this.refs.recordLabelTitle).value
        });

        $('.recordLabelInput')
            .search({
                source: this.data.recordLabels,
                fields: {
                    title: 'fullName'
                },
                searchFields: [
                    'fullName'
                ],
                searchFullText: false,
                onSelect: function (result, response) {
                    //console.log(result, response);
                    var recordLabelData = {
                        _id: result._id,
                        slug: result.slug,
                        title: result.fullName
                    };
                    root.props.handleChange(recordLabelData);
                    root.setState({
                        recordLabelTitle: result.fullName,
                        recordLabel: recordLabelData
                    });
                },
                onSearchQuery: function (query) {
                    if (query !== root.state.recordLabel.fullName) {
                        var recordLabelData = {
                            _id: null,
                            slug: null,
                            title: query
                        };
                        root.props.handleChange(recordLabelData);
                        //console.log('set as unknown artist', query);
                        root.setState({
                            recordLabel: recordLabelData
                        });
                    }
                }
            });
    },

    render() {
        return <div className="ui search recordLabelInput">
            <input
                ref="recordLabelTitle"
                name="recordLabelTitle"
                onChange={this.updateRecordLabel}
                value={this.state.recordLabelTitle}
                className="prompt"
                type="text"
                placeholder="Album record label" />
            <div className="results"></div>
        </div>
    }
});
