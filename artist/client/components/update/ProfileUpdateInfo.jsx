ProfileUpdateInfo = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        artist: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            title: this.props.artist.title || '',
            fullName: this.props.artist.fullName || '',
            slug: this.props.artist.slug || '',
            biography: this.props.artist.biography || ''
        };
    },

    componentDidMount() {
        $('select.dropdown').dropdown();

        $('.ui.form')
            .form({
                fields: {
                    title: {
                        identifier: 'title',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Title"'
                            }
                        ]
                    },
                    fullName: {
                        identifier: 'fullName',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Full Name"'
                            }
                        ]
                    },
                    slug: {
                        identifier: 'slug',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Slug"'
                            }
                        ]
                    }
                }
            });
    },

    handleChange(e) {
        this.setState({
            title: ReactDOM.findDOMNode(this.refs.title).value,
            fullName: ReactDOM.findDOMNode(this.refs.fullName).value,
            slug: ReactDOM.findDOMNode(this.refs.slug).value,
            biography: ReactDOM.findDOMNode(this.refs.biography).value
        });
    },

    handleBlur() {
        this.updateWithNewData({
            title: ReactDOM.findDOMNode(this.refs.title).value.trim(),
            fullName: ReactDOM.findDOMNode(this.refs.fullName).value.trim(),
            slug: ReactDOM.findDOMNode(this.refs.slug).value.trim(),
            biography: ReactDOM.findDOMNode(this.refs.biography).value.trim()
        });
    },

    updateWithNewData(newData) {
        //console.log('update profile: ', newData);

        Meteor.call('updateArtist', newData, function (err, res) {
            console.log(err, res);
        });
    },

    handleSubmit(event, template) {
        event.preventDefault();
        console.log('handle submit');
        this.handleBlur();

        FlashMessages.sendSuccess('Thanks, your settings have been saved.');
    },

    render() {
        return <form className="ui form" onSubmit={this.handleSubmit}>
            <ArtistPictureUpdate artist={this.props.artist} />
            <div className="field">
                <label>What is your title? *</label>
                <select
                    name="title"
                    ref="title"
                    className="ui fluid dropdown"
                    value={this.state.title}
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}>
                    <option value="listener">Listener/Fan</option>
                    <option value="artist">Artist/Singer</option>
                    <option value="record-label">Record Label</option>
                    <option value="writer">Song Writer</option>
                    <option value="music-producer">Music Producer</option>
                    <option value="video-director">Video Director</option>
                    <option value="model">Actor/Model</option>
                    <option value="other">Other</option>
                </select>
            </div>
            <div className="field">
                <label>Your Full Name *</label>
                <input
                    name="fullName"
                    ref="fullName"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.fullName}
                    placeholder="Your Full Name"/>
            </div>
            <div className="field">
                <label>Your Biography</label>
                <textarea
                    rows="2"
                    name="biography"
                    ref="biography"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.biography}
                    placeholder="Tell the world a little bit about yourself."></textarea>
            </div>
            <div className="field">
                <label>Your Web Page URL *</label>
                <div className="ui labeled input">
                    <div className="ui label">
                        http://www.midtube.com/
                    </div>
                    <input
                        name="slug"
                        ref="slug"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        value={this.state.slug}
                        placeholder="my-name"/>
                </div>
            </div>
            <button className="ui labeled icon blue button" type="submit">
                <i className="save icon"></i>
                Submit
            </button>
            <div className="ui error message"></div>
        </form>
    }
});
