ProfileUpdate = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData, SmoothScrollMixin],

    componentDidMount: function() {
        $('.tabular .item').tab();

        SEO.set({
            title: 'Update Artist Profile',
            description: 'Description for this template',
            meta: {
                //'property="og:image"': 'http://locationofimage.com/image.png'
            }
        });
    },

    getMeteorData() {
        Meteor.subscribe('userData');

        return {
            artist: Artist.findOne({userId: Meteor.userId()}),
            user: Meteor.user()
        }
    },

    render() {
        var payment = this.data.user.payment || {};

        return <div className="main">
            <PageHeader title="Profile Update" />
            <div className="ui grid container main tabular stackable">
                <div className="four wide column">
                    <div className="ui vertical small steps" style={{width: '100%'}}>
                        <a className="active step item" data-tab="info">
                            <i className="info icon"></i>
                            <div className="content">
                                <div className="title">Profile Info</div>
                            </div>
                        </a>
                        <a className="step item" data-tab="social">
                            <i className="share alternate icon"></i>
                            <div className="content">
                                <div className="title">Social</div>
                            </div>
                        </a>
                        <a className="step item" data-tab="password">
                            <i className="privacy icon"></i>
                            <div className="content">
                                <div className="title">Email/Password</div>
                            </div>
                        </a>
                        <a className="step item" data-tab="payments">
                            <i className="payment icon"></i>
                            <div className="content">
                                <div className="title">Payments/Billing</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div className="twelve wide column">
                    <div className="ui active tab segment" data-tab="info">
                        <ProfileUpdateInfo artist={this.data.artist} />
                    </div>
                    <div className="ui tab segment" data-tab="social">
                        <ProfileUpdateSocial artist={this.data.artist} />
                    </div>
                    <div className="ui tab segment" data-tab="password">
                        <UserEmailUpdate user={this.data.user} />
                        <ChangePasswordWrapper />
                    </div>
                    <div className="ui tab segment" data-tab="payments">
                        <UserPaymentUpdate payment={payment} />
                    </div>
                </div>
            </div>
        </div>
    }
});
