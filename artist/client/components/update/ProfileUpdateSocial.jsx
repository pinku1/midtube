ProfileUpdateSocial = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        artist: React.PropTypes.object.isRequired
    },

    getInitialState() {
        if (_.isUndefined(this.props.artist.social)) {
            this.props.artist['social'] = {};
        }
        if (_.isUndefined(this.props.artist.contact)) {
            this.props.artist['contact'] = {};
        }

        return {
            twitter: this.props.artist.social.twitter || '',
            facebook: this.props.artist.social.facebook || '',
            instagram: this.props.artist.social.instagram || '',
            mobile: this.props.artist.contact.mobile || '',
            landline: this.props.artist.contact.landline || '',
            address: this.props.artist.contact.address || '',
            website: this.props.artist.contact.website || ''
        };
    },

    handleChange(e) {
        this.setState({
            twitter: ReactDOM.findDOMNode(this.refs.twitter).value,
            facebook: ReactDOM.findDOMNode(this.refs.facebook).value,
            instagram: ReactDOM.findDOMNode(this.refs.instagram).value,
            mobile: ReactDOM.findDOMNode(this.refs.mobile).value,
            address: ReactDOM.findDOMNode(this.refs.address).value,
            website: ReactDOM.findDOMNode(this.refs.website).value
        });
    },

    handleBlur() {
        this.updateWithNewData({
            twitter: ReactDOM.findDOMNode(this.refs.twitter).value.trim(),
            facebook: ReactDOM.findDOMNode(this.refs.facebook).value.trim(),
            instagram: ReactDOM.findDOMNode(this.refs.instagram).value.trim(),
            mobile: ReactDOM.findDOMNode(this.refs.mobile).value.trim(),
            address: ReactDOM.findDOMNode(this.refs.address).value.trim(),
            website: ReactDOM.findDOMNode(this.refs.website).value.trim()
        });
    },

    updateWithNewData(newData) {
        //console.log('update profile: ', newData);

        Meteor.call('updateArtist', {
            social: {
                twitter: newData.twitter,
                facebook: newData.facebook,
                instagram: newData.instagram
            },
            contact: {
                mobile: newData.mobile,
                landline: newData.landline,
                address: newData.address,
                website: newData.website
            }
        });
    },

    handleSubmit(event, template) {
        event.preventDefault();
        console.log('handle submit');
        this.handleBlur();

        FlashMessages.sendSuccess('Thanks, your settings have been saved.');
    },

    render() {
        return <form className="ui form" onSubmit={this.handleSubmit}>
            <div className="field">
                <label>Your Twitter Profile URL</label>
                <input
                    name="twitter"
                    ref="twitter"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.twitter}
                    placeholder="https://twitter.com/justinbieber"/>
            </div>
            <div className="field">
                <label>Your Facebook Profile URL</label>
                <input
                    name="facebook"
                    ref="facebook"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.facebook}
                    placeholder="https://www.facebook.com/JustinBieber"/>
            </div>
            <div className="field">
                <label>Your Instagram Profile URL</label>
                <input
                    name="instagram"
                    ref="instagram"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.instagram}
                    placeholder="https://instagram.com/justinbiebertracker/"/>
            </div>
            <div className="field">
                <label>Your Mobile Number URL</label>
                <input
                    name="mobile"
                    ref="mobile"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.mobile}
                    placeholder="074112345898"/>
            </div>
            <div className="field">
                <label>Your Website URL</label>
                <div className="ui labeled input">
                    <div className="ui label">
                        http://
                    </div>
                    <input
                        name="website"
                        ref="website"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        value={this.state.website}
                        placeholder="mysite.com"/>
                </div>
            </div>

            <div className="field">
                <label>Your Current Location</label>
                <div className="ui left icon input">
                    <input
                        name="address"
                        ref="address"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        value={this.state.address}
                        placeholder="Birmingham, UK"/>
                    <i className="location arrow icon"></i>
                </div>
            </div>
            <button className="ui labeled icon blue button" type="submit">
                <i className="save icon"></i>
                Submit
            </button>
        </form>
    }
});
