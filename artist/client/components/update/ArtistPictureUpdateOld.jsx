ArtistPictureUpdateOld = React.createClass({
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        artist: React.PropTypes.object.isRequired
    },

    getMeteorData() {
        if (_.isUndefined(this.props.artist.profile_picture)) {
            this.props.artist['profile_picture'] = {};
        }

        return {
            //image: this.props.artist.profile_picture.getFileRecord()
            image: Images.findOne({_id: this.props.artist.profile_picture._id})
        };
    },

    uploadFile(event) {
    	var artistId = this.props.artist._id;
        //var fsFile = new FS.File(event.target.files[0]);
        var fsFile = event.target.files[0];
        //fsFile.owner = artistId;
        //fsFile.metadata.type = 'artist';
        Images.insert(fsFile, function (err, fileObj) {
            Meteor.call('updateArtistPicture', artistId, fileObj, function () {
                console.log('all done');
            });
        });
    },

    renderImage() {
    	if (!_.isEmpty(this.data.image)) {
	    	return <div>
	            <a href={this.data.image.url({store:'images'})} target="_blank">
	                <img src={this.data.image.url({store:'thumbs'})} />
	            </a>
	        </div>
    	} else {
    		return <div>
    			no image uploaded
    		</div>
    	}
    },

    render() {
        return <div>
            <input ref="file" type="file" name="file" onChange={this.uploadFile} />
            {this.renderImage()}
        </div>
    }
});
