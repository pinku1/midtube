ArtistPictureUpdate = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        artist: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            progress: 0,
            isLoading: false
        };
    },

    uploadFile(event) {
        var root = this;
        var metaContext = {artistId: this.props.artist._id, type: 'artist'};
        var uploader = new Slingshot.Upload("imageUploads", metaContext);
        var file = event.target.files[0];
        //show loading
        this.setState({isLoading: true});

        //progress bar
        var computation = Tracker.autorun(function () {
            var progress = Math.ceil(uploader.progress() * 100);
            if (progress === parseInt(progress, 10)) {
                root.setState({progress: progress});
            }
        });

        console.log(file);
        async.series([
                function(callback){
                    //upload original
                    uploader.send(file, function (error, downloadUrl) {
                        if (error) {
                            // Log service detailed response.
                            console.error('Error uploading', uploader.xhr.response);
                            console.log(error);
                        }
                        else {
                            console.log(downloadUrl);

                            callback(error, {size: file.size, type: file.type, pathOriginal: downloadUrl});
                        }
                    });
                },
                function(callback){
                    //upload resized
                    Resizer.resize(file, {width: 400, height: 400, cropSquare: true}, function(err, fileCropped) {
                        uploader.send(fileCropped, function (error, downloadUrl) {
                            if (error) {
                                // Log service detailed response.
                                console.error('Error uploading', uploader.xhr.response);
                                console.log(error);
                            }
                            else {
                                console.log(downloadUrl);

                                callback(error, {path: downloadUrl});
                            }
                        });

                    });
                }
            ],
            function(err, results){
                computation.stop(); // Stop the computation in order to save memory.

                Meteor.call('updateArtist', {profilePicture: _.extend(results[0], results[1])}, function () {
                    console.log('all done');
                    //hide loading
                    root.setState({isLoading: false});
                    FlashMessages.sendSuccess('Picture uploaded successfully!');
                });
            });
    },

    renderProgressBar() {
        var progressStyle = {transitionDuration: '300ms', width: this.state.progress + '%'};
        var progressClass = classNames({
            'ui': true,
            'active': this.state.progress > 0,
            'progress': true,
            'success': this.state.progress === 100,
            'hidden': this.state.progress === 0
        });

        return <div className={progressClass} data-percent={this.state.progress}>
            <div className="bar" style={progressStyle}>
                <div className="progress">{this.state.progress}</div>
            </div>
            <div className="label">Uploading Files</div>
        </div>
    },

    triggerInput() {
        $('#artistPictureInput').click();
    },

    render() {
        return <div>
            <Loading active={this.state.isLoading} />
            <ArtistImage artist={this.props.artist} />
            {this.renderProgressBar()}
            <button className="ui primary button" onClick={this.triggerInput} style={{'margin': '10px 0'}}>
                <i className="upload icon"></i> Upload Picture
            </button>
            <input
                ref="file"
                type="file"
                name="file"
                id="artistPictureInput"
                onChange={this.uploadFile}
                style={{'display': 'none'}} />
        </div>
    }
});
