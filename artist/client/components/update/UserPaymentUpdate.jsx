UserPaymentUpdate = React.createClass({
    propTypes: {
        payment: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            paypalId: this.props.payment.paypalId || '',
            bankCheckName:  this.props.payment.bankCheckName || ''
        };
    },

    componentDidMount() {
        //todo trick! fix it or remove it
        var root = this;
        setTimeout(function() {
            root.setState({
                paypalId: root.props.payment.paypalId || '',
                bankCheckName:  root.props.payment.bankCheckName || ''
            });
        }, 1000);
    },

    handleChange(e) {
        this.setState({
            paypalId: ReactDOM.findDOMNode(this.refs.paypalId).value,
            bankCheckName: ReactDOM.findDOMNode(this.refs.bankCheckName).value
        });
    },

    handleBlur() {
        this.updateWithNewData({
            paypalId: ReactDOM.findDOMNode(this.refs.paypalId).value.trim(),
            bankCheckName: ReactDOM.findDOMNode(this.refs.bankCheckName).value.trim()
        });
    },

    updateWithNewData(newData) {
        //console.log('update profile: ', newData);

        Meteor.call('updateUserPayment', newData);
    },

    handleSubmit(event, template) {
        event.preventDefault();
        console.log('handle submit');
        this.handleBlur();

        FlashMessages.sendSuccess('Thanks, your payment settings have been updated.');
    },

    render() {
        return (
            <div>
                <div className="ui piled segment">
                    <h4 className="ui header">
                        Your current balance: $0
                    </h4>
                    <p>
                        Please type in below one of your payment details, where you would like to get paid.<br />
                        Minimum payment amount is $25 and will be paid monthly on 30th of the month.
                    </p>
                </div>
                <form className="ui form" onSubmit={this.handleSubmit}>
                    <div className="field">
                        <label>Your Paypal Account for payments</label>
                        <input
                            name="paypalId"
                            ref="paypalId"
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            value={this.state.paypalId}
                            placeholder="Your Paypal Account Details"/>
                    </div>
                    <div className="field">
                        <label>Cheque Payee Name for payments</label>
                        <input
                            name="bankCheckName"
                            ref="bankCheckName"
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            value={this.state.bankCheckName}
                            placeholder="Bank Cheque Payee Name for payments"/>
                    </div>
                    <button className="ui labeled icon blue button" type="submit">
                        <i className="save icon"></i>
                        Submit
                    </button>
                </form>
            </div>
        )
    }
});
