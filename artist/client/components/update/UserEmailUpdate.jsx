UserEmailUpdate = React.createClass({
    propTypes: {
        user: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            email: !_.isUndefined(this.props.user.emails) ? this.props.user.emails[0].address || '' : ''
        };
    },

    componentDidMount() {
        $('.ui.form.email')
            .form({
                fields: {
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Email"'
                            }
                        ]
                    }
                }
            });
    },

    handleChange(e) {
        this.setState({
            email: ReactDOM.findDOMNode(this.refs.email).value
        });
    },

    handleBlur() {
        this.updateWithNewData({
            email: ReactDOM.findDOMNode(this.refs.email).value.trim()
        });
    },

    updateWithNewData(newData) {
        //console.log('update profile: ', newData);

        Meteor.call('updateUserEmail', newData);
    },

    handleSubmit(event, template) {
        event.preventDefault();
        console.log('handle submit');
        this.handleBlur();

        FlashMessages.sendSuccess('Thanks, your settings have been saved.');
    },

    render() {
        return (
            <form className="ui form email" onSubmit={this.handleSubmit}>
                <div className="field">
                    <label>Update Your Email *</label>
                    <input
                        name="email"
                        ref="email"
                        type="email"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        value={this.state.email}
                        placeholder="Your email address"/>
                </div>
                <button className="ui labeled icon blue button" type="submit">
                    <i className="save icon"></i>
                    Update
                </button>
                <div className="ui error message"></div>
            </form>
        )
    }
});
