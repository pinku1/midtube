SearchArtistInput = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        artists: React.PropTypes.array.isRequired,
        handleChange: React.PropTypes.func
    },

    getInitialState() {
        return {
            artistName: this.props.artists ? !_.isUndefined(this.props.artists[0]) ? this.props.artists[0].fullName : '' : '',
            artist: this.props.artists ? !_.isUndefined(this.props.artists[0]) ? this.props.artists[0] : {} : {}
        };
    },

    getMeteorData() {
        var title = 'artist';
        Meteor.subscribe('getArtistByTitle', title);

        return {
            singers: Artist.find({title: title}, {sort: {title: 1}}).fetch()
        };
    },

    updateArtist() {
        var root = this;
        this.setState({
            artistName: ReactDOM.findDOMNode(this.refs.artistName).value
        });

        $('.artistInput')
            .search({
                source: this.data.singers,
                fields: {
                    title: 'fullName'
                },
                searchFields: [
                    'fullName'
                ],
                searchFullText: false,
                onSelect: function (result, response) {
                    //console.log(result, response);
                    var artistData = {
                        _id: result._id,
                        slug: result.slug,
                        fullName: result.fullName
                    };
                    root.props.handleChange([artistData]);
                    root.setState({
                        artist: artistData,
                        artistName: result.fullName
                    });
                },
                onSearchQuery: function (query) {
                    if (query !== root.state.artist.fullName) {
                        var artistData = {
                            _id: null,
                            slug: null,
                            fullName: query
                        };
                        root.props.handleChange([artistData]);
                        //console.log('set as unknown artist', query);
                        root.setState({
                            artist: artistData
                        });
                    }
                }
            });
    },

    render() {
        return <div className="ui search artistInput">
            <input
                ref="artistName"
                name="artistName"
                onChange={this.updateArtist}
                value={this.state.artistName}
                className="prompt"
                type="text"
                placeholder="Album artist name" />
            <div className="results"></div>
        </div>
    }
});
