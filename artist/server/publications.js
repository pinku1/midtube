Meteor.publish('userProfile', function(slug, userId) {
    var selector;
    if (!_.isNull(slug)) {
        selector = {slug: slug};
    } else if (!_.isUndefined(userId)) {
        selector = {userId: userId};
    } else {
        selector = {userId: this.userId};
    }

    return Artist.find(selector);
});

Meteor.publish('findArtists', function (query, limit) {
    //find by query
    var find = {};

    if (!_.isNull(query)) {
        var queryRegex = ".*" + query + ".*";
        find = {
            $or: [
                {fullName: {$regex: queryRegex, $options: 'i'}},
                {biography: {$regex: queryRegex, $options: 'i'}}
            ]
        };
    }

    if (_.isUndefined(limit)) {
        limit = AppSettings.limit;
    }

    if (limit > 300) {
        limit = 300;
    }

    var searchCount = find;
    //don't count current user fix
    if (!_.isNull(this.userId)) {
        searchCount = _.extend(searchCount, {userId: {'$ne': this.userId}});
    }

    Counts.publish(this, 'total-artists-count', Artist.find(searchCount));

    return Artist.find(find, {sort: {createdAt: -1, baseScore: -1}, limit: parseInt(limit)});
});

Meteor.publish('allArtistAlbums', function(artistId, userId) {
    var where = {isActive: true};
    if (!_.isNull(userId)) {
        where = _.extend(where, {'$or': [{ownerId: userId}, {artists: {'$elemMatch': {_id: artistId}}}]});
    } else {
        where = _.extend(where, {artists: {'$elemMatch': {_id: artistId}}});
    }
    return Album.find(where, {sort: {createdAt: -1}, limit: 60});
});

Meteor.publish('favoriteAlbums', function(userId) {
    if (_.isNull(userId) || _.isUndefined(userId)) {
        return;
    }
    return Album.find({favoriteBy: userId, isActive: true}, {sort: {createdAt: -1}, limit: 60});
});

Meteor.publish('featuredArtists', function() {
    //show featured artists only
    return Artist.find({featured: true}, {sort: {createdAt: -1}, limit: 4});
});

Meteor.publish('getArtistByTitle', function (category) {
    //find by query
    var find = {};

    //get by category (title)
    if (!_.isUndefined(category)) {
        find['title'] = category;
    }

    //console.log(find);
    return Artist.find(find, {sort: {createdAt: -1}, limit: 100});
});

Meteor.publish('userData', function() {
    if (this.userId) {
        return Meteor.users.find({_id: this.userId},
            {fields: {emails: 1, profile: 1, payment: 1}});
    } else {
        this.ready();
    }
});

Meteor.publish('userAlbums', function() {
    return Album.find({ownerId: this.userId}, {sort: {createdAt: -1}});
});
