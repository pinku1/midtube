Slingshot.fileRestrictions("imageUploads", {
    allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
    maxSize: 10 * 1024 * 1024 // 10 MB (use null for unlimited).
});

Slingshot.fileRestrictions("songUploads", {
    allowedFileTypes: ["audio/mp3", "audio/mpeg3", "audio/x-mpeg-3"],
    maxSize: 100 * 1024 * 1024 // 100 MB (use null for unlimited).
});
