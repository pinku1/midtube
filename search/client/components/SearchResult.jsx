SearchResult = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData, SmoothScrollMixin],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        query: React.PropTypes.string
    },

    getMeteorData() {
        return {
            artistsCount: Counts.get('total-artists-count'),
            albumsCount: Counts.get('total-albums-count')
        }
    },

    componentDidMount: function() {
        SEO.set({
            title: 'Search Results',
            description: 'Description for this template',
            meta: {
              //'property="og:image"': 'http://locationofimage.com/image.png'
            }
          });

        $('.menu .item').tab();
    },

    render() {
        return (
            <div className="ui grid container main">
                <PageHeader title={'Search result for ' + this.props.query} />
                <div className="ui divider" style={{'width': '100%'}}></div>

                <h3 className="ui header" style={{'width': '100%'}}>Albums ({this.data.albumsCount})</h3>
                <div className="ui segment" style={{'width': '100%'}}>
                    <AlbumResult />
                </div>
                <h3 className="ui header" style={{'width': '100%'}}>Artists ({this.data.artistsCount})</h3>
                <div className="ui segment" style={{'width': '100%'}}>
                    <ArtistResult />
                </div>
            </div>
        )
    },

    renderOld() {
        return <div className="main">
            <PageHeader title={'Search result for ' + this.props.query} />
            <div className="ui grid container main">
                <div className="ui secondry menu attached top pointing">
                  <div className="item active" data-tab="albums">Albums ({this.data.albumsCount})</div>
                  <div className="item" data-tab="artists">Artists ({this.data.artistsCount})</div>
                </div>
                <div className="ui tab segment attached bottom active" data-tab="albums">
                    <AlbumResult />
                </div>
                <div className="ui tab segment attached bottom" data-tab="artists">
                    <ArtistResult />
                </div>
            </div>
        </div>
    }
});
