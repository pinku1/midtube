SearchForm = React.createClass({
    componentDidMount: function () {
        $('.siteSearch').on('keydown', this.handleKeyDown);
    },

    componentWillUnmount: function () {
        $('.siteSearch').off('keydown', this.handleKeyDown);
    },

    getInitialState() {
        return {
            query: FlowRouter.getParam('query') || ''
        };
    },

    handleChange(e) {
        this.setState({
            query: ReactDOM.findDOMNode(this.refs.query).value
        });
    },

    handleBlur() {
        this.doSearch(ReactDOM.findDOMNode(this.refs.query).value.trim());
    },

    handleSubmit(event, template) {
        event.preventDefault();
        //console.log('handle submit');

        this.doSearch(this.state.query);
    },

    handleKeyDown: function (e) {
        var root = this;
        var ENTER = 13;
        if (e.keyCode == ENTER) {
            this.doSearch(this.state.query);
        }

        //else do algolia
        AlgoliaIndex.search(this.state.query, function (error, content) {
            if (error) {
                console.error('Error:', error);
            } else {
                //console.log('Content:', content);
            }

            //var contentResult = content.hits.map(function (item) {
            //    return {title: item.title};
            //});

            //console.log(contentResult);
            $('.siteSearch').search({
                source: content.hits,
                minCharacters : 2,
                maxResults: 12,
                cache: false,
                searchFullText: true,
                searchFields: [
                    'title', 'description', 'tags', 'albumArtistName', 'albumWriterName'
                ],
                fields: {
                    categories      : 'results',     // array of categories (category view)
                    categoryName    : 'name',        // name of category (category view)
                    categoryResults : 'results',     // array of results (category view)
                    description     : 'descriptionNO', // result description
                    image           : 'image',       // result image
                    price           : 'price',       // result price
                    results         : 'results',     // array of results (standard)
                    title           : 'title',       // result title
                    action          : 'action',      // "view more" object name
                    actionText      : 'text',        // "view more" text
                    actionURL       : 'url'          // "view more" url
                },
                onSelect: function (result, response) {
                    root.setState({
                        query: result.title
                    });
                }
            });
        });
    },

    doSearch(query) {
        //only search if search term is more then 2 chars
        if (query.length > 2) {
            FlowRouter.go('/search/:query', {query: query});
        }
    },

    render() {
        return <div className="ui fluid icon input search siteSearch">
            <input
                ref="query"
                name="query"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                value={this.state.query}
                placeholder="Search albums or artists..."
                className="prompt"
                style={{'backgroundColor': '#F6F6F6', 'borderRadius': '4px'}} />
            <div className="results"></div>
            <i className="search link icon" onClick={this.handleSubmit}></i>
        </div>
    }
});
