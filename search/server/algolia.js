var client = AlgoliaSearch('M2XJNY15P8', 'af1e4a74e75b8736743c2874eb412e5c');
var AlgoliaIndex = client.initIndex('midtube_global');

//to manually add, todo can be removed
addAlgoliaIndex = function (array) {
    // array contains the data you want to save in the index
    //array = [{objectID: '123435asdfsdaf', title: 'Album One', score: 10}];
    //console.log(array);
    AlgoliaIndex.saveObjects(array, function (error, content) {
        if (error) console.error('Error:', error);
        else console.log('Content:', content);
    });
};
//addAlgoliaIndex();

var transformAlbum = function (doc) {
    return {
        _id: doc._id,
        title: doc.title,
        description: doc.description,
        score: doc.baseScore,
        votes: doc.upVotes,
        tags: doc.tags,
        albumArtistName: !_.isUndefined(doc.artists[0]) ? doc.artists[0].fullName : '',
        albumWriterName: doc.writerName
    };
};

var transformArtist = function (doc) {
    return {
        _id: doc._id,
        title: doc.fullName,
        description: doc.biography,
        score: doc.baseScore,
        votes: doc.subscribers
    };
};

//auto sync album collection
Albums.syncAlgolia(AlgoliaIndex, {
    debug: false,
    transform: function(doc) {
        return transformAlbum(doc);
    }
});

//auto sync artists collection
Artists.syncAlgolia(AlgoliaIndex, {
    debug: false,
    transform: function(doc) {
        return transformArtist(doc);
    }
});

//Clean sync, do one time only
Meteor.methods({
    'reSyncAlgolia': function () {
        Albums.initAlgolia(AlgoliaIndex, {
            clearIndex: true,
            debug: true,
            transform: function(doc) {
                return transformAlbum(doc);
            }
        });

        Artists.initAlgolia(AlgoliaIndex, {
            clearIndex: false,
            debug: true,
            transform: function(doc) {
                return transformArtist(doc);
            }
        });
    }
});
