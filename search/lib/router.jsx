FlowRouter.route('/search/:query', {
    name: 'search',
    subscriptions: function(params, queryParams) {
        var albumLimit = parseInt(queryParams['album-limit']) || AppSettings.limit;
        var artistLimit = parseInt(queryParams['artist-limit']) || AppSettings.limit;
        this.register('findAlbums', Meteor.subscribe('findAlbums', params.query, albumLimit));
        this.register('findArtists', Meteor.subscribe('findArtists', params.query, artistLimit));
    },
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <SearchResult query={params.query} />
        });
    }
});
