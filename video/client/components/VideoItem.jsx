VideoItem = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        video: React.PropTypes.object.isRequired
    },

    getMeteorData() {
        return {
            currentUserId: Meteor.userId()
        };
    },

    componentDidMount() {
        $('.video-dropdown').dropdown();
    },

    getThumbnail() {
        if (!_.isUndefined(this.props.video.thumbnail) && this.props.video.thumbnail.length > 0) {
            return this.props.video.thumbnail;
        } else {
            return '/images/video.jpg';
        }
    },

    _onPlay(event) {
        //console.log(event.target);
        // access to player in all event handlers via event.target
        //event.target.pauseVideo();
    },

    renderVideoModal() {
        const opts = {
            height: '390',
            width: '640',
            playerVars: { // https://developers.google.com/youtube/player_parameters
                autoplay: 0
            }
        };

        return <div className="ui basic modal" id={'videoModal' + this.props.video._id}>
            <i className="close icon"></i>

            <div className="content">
                <YouTube
                    url={'http://www.youtube.com/watch?v=' + this.props.video.youtubeKey}
                    opts={opts}
                    onPlay={this._onPlay}
                    />
            </div>
        </div>
    },

    openModal() {
        var root = this;
        $('#videoModal' + this.props.video._id)
            .modal({detachable: false, autofocus: true})
            .modal('show')
            .modal({onHide: function () {
                //todo pause video on hide
                console.log('is hiding!!!');
            }});
    },

    isOwner() {
        return this.data.currentUserId === this.props.video.ownerId;
    },

    deleteItem(ev) {
        Meteor.call('removeVideo', this.props.video._id);
    },

    render() {
        return <div className="column">
            <div className="ui card">
                <a className="image" onClick={this.openModal}>
                    <img src={this.getThumbnail()}/>
                </a>

                <div className="content">
                    <a className="header" onClick={this.openModal} style={{'fontWeight': '300', 'fontSize': '1.1rem'}}>
                        {truncate(this.props.video.title, 15)}
                    </a>
                </div>
                { this.isOwner() ?
                    <div className="ui icon top left pointing dropdown float button video-dropdown">
                        <i className="ellipsis vertical icon"></i>

                        <div className="menu">
                            <a className="item" onClick={this.deleteItem}>Delete</a>
                        </div>
                    </div>
                    : ''
                }
            </div>
            {this.renderVideoModal()}
        </div>
    }
});
