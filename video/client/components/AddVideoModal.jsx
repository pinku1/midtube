AddVideoModal = React.createClass({
    getInitialState() {
        return {
            youtubeLink: ''
        };
    },

    handleChange(e) {
        this.setState({
            youtubeLink: ReactDOM.findDOMNode(this.refs.youtubeLink).value.trim()
        });
    },

    handleSubmit(ev) {
        var root = this;

        Meteor.call('addVideo', this.state.youtubeLink, function (err, result) {
            if (!err) {
                root.setState({
                    youtubeLink: ''
                });
            }
        });

        $('#addVideoModal').modal('hide');
    },

    render() {
        return <div className="ui modal" id="addVideoModal">
            <i className="close icon"></i>
            <div className="header">
                Add video
            </div>
            <div className="content">
                <form className="ui form">
                    <div className="field">
                        <label>Youtube Video Link</label>
                        <input
                            type="text"
                            name="youtubeLink"
                            ref="youtubeLink"
                            onChange={this.handleChange}
                            value={this.state.youtubeLink}
                            placeholder="Youtube video link" />
                    </div>
                </form>
            </div>
            <div className="actions">
                <div className="ui labeled icon button" onClick={this.handleSubmit}>
                    <i className="right arrow icon"></i>
                    Submit
                </div>
            </div>
        </div>
    }
});
