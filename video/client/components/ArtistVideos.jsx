ArtistVideos = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        artist: React.PropTypes.object.isRequired,
        isOwner: React.PropTypes.bool
    },

    getMeteorData() {
        var subscription = Meteor.subscribe('userVideos', this.props.artist.userId);

        return {
            loading: ! subscription.ready(),
            videos: Video.find({ownerId: this.props.artist.userId}, {'limit': 60, sort: {createdAt: -1}}).fetch()
        };
    },

    renderVideos () {
        return this.data.videos.map((video) => {
            return <VideoItem
                key={video._id}
                video={video} />
        });
    },

    addVideo() {
        $('#addVideoModal')
            .modal({detachable: false, autofocus: true})
            .modal('show');
    },

    render() {
        return <div>
            <Loading active={this.data.loading} />
            <div className="ui grid container">
                <h2 className="ui header">
                    <ArtistTitle title={this.props.artist.title} />'s Videos
                </h2>
                {this.props.isOwner ?
                <button className="circular ui icon button right floated green" style={{'height': '34px'}} onClick={this.addVideo}>
                    <i className="icon add"></i>
                </button>
                    : '' }
                <AddVideoModal />
            </div>
            <div className="ui doubling centered six column grid container">
                { this.data.videos.length > 0 ?
                    this.renderVideos() :
                    <div className="ui message">
                        <div className="header">
                            No videos for artist found
                        </div>
                    </div>
                }
            </div>
        </div>
    }
});
