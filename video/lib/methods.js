Meteor.methods({
    'removeVideo': function(docId) {
        if (_.isNull(this.userId)) {
            //if not logged in
            throw new Meteor.Error('not-logged-in', 'Please login to continue.');
        }

        //check if update is owner of video
        return Video.remove({_id: docId, ownerId: this.userId});
    }
});
