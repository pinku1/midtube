var requiredString = Validators.and([
    Validators.required(),
    Validators.string(),
    Validators.minLength(3),
    Validators.maxLength(40)
]);
var requiredDate = Validators.and([
    Validators.required(),
    Validators.date()
]);

Videos = new Mongo.Collection('videos');

Video = Astro.Class({
    name: 'Video',
    collection: Videos,
    fields: {
        title: {
            type: 'string',
            validator: requiredString
        },
        description: 'string',
        featured: {
            type: 'boolean',
            default: false
        },
        youtubeKey: 'string',
        thumbnail: 'string',
        ownerId: 'string',
        isActive: {
            type: 'boolean',
            default: true
        },
        tags: {
            type: 'array',
            default: function() {
                return [];
            }
        }
    },
    behaviors: {
        timestamp: {
            hasCreatedField: true,
            createdFieldName: 'createdAt',
            hasUpdatedField: true,
            updatedFieldName: 'updatedAt'
        }
    }
});
