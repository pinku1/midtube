Meteor.methods({
    'addVideo': function (youtubeLink) {
    	var videoKey = null;
    	var ownerId = this.userId;
    	var videoData = null;

		return Meteor.sync(function(done) {
	    	async.series([
			    function(callback){
			    	//get video key from url 
					var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
					var match = youtubeLink.match(regExp);
					if (match && match[2].length == 11) {
					  	videoKey = match[2];
			        	callback(null, 'one');
					} else {
						callback(true, null);
					}
			    },
			    function(callback){
					//get video details
			    	Youtube.authenticate({
					    type: "key", 
					    key: "AIzaSyBUlusESC--7NNtohKrR3lfBXPujJcjPaw"
					});
					Youtube.videos.list({id: videoKey, part: 'snippet'}, function (err, data) {
				    	videoData = data.items[0];
		        		callback(err, data);
					});
			    },
			    Meteor.bindEnvironment(function(callback){
					//insert into database
					var video = new Video();
                    video.set({
				    	ownerId: ownerId,
						youtubeKey: videoData.id,
						title: videoData.snippet.title,
						description: videoData.snippet.description,
						tags: videoData.snippet.tags,
						thumbnail: videoData.snippet.thumbnails.high.url
				    });
                    video.save();
	        		callback(null, video);
			    })
			],
			// optional callback
			function(err, results){
			    // results is now equal to ['one', 'two']
			    //console.log(err, results);
	        	done({}, results[2]);
			});
	    });	
    }
});
