Meteor.publish('userVideos', function(userId) {
    return Video.find({ownerId: userId}, {'limit': 60, sort: {createdAt: -1}});
});
