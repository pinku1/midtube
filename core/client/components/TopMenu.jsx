TopMenuDropDown = React.createClass({
    propTypes: {
        currentUser: React.PropTypes.object.isRequired,
        profile: React.PropTypes.object.isRequired
    },

    initDropDown() {
        $('#profileDropdown').dropdown('refresh');
    },

    componentDidMount() {
        this.initDropDown();
    },

    componentDidUpdate() {
        this.initDropDown();
    },

    logOut() {
        AccountsTemplates.logout();
    },

    profileUrl() {
        if (!_.isUndefined(this.props.profile)) {
            return FlowRouter.path('/profile/:slug', {slug: this.props.profile.slug});
        }
    },

    render() {
        return <div className="ui dropdown item" id="profileDropdown">
            <i className="user icon"></i> Profile <i className="dropdown icon"></i>
            <div className="menu">
                <a className="item" href={this.profileUrl()}>
                    <i className="user icon"></i>Profile
                </a>
                <a className="item" href="/profile/update">
                    <i className="edit icon"></i>My Account
                </a>
                <a className="item" href="/profile/music">
                    <i className="sound icon"></i>My Music
                </a>
                <a className="item" onClick={this.logOut}>
                    <i className="sign out icon"></i>Log Out
                </a>
            </div>
        </div>
    }
});

TopMenu = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    componentDidMount() {
        Session.setDefault('defaultCategory', '.com');

        $('.ui.dropdown.logo').dropdown({
            action: 'hide',
            onChange: function(text, value, $selectedItem) {
                // nothing built in occurs
                //console.log(text, value, $selectedItem);
                Session.set('defaultCategory', text);
                FlashMessages.send('Success!');
            }
        });
    },

    getMeteorData() {
        //get current user artist profile
        Meteor.subscribe('userProfile', null);
        Meteor.subscribe('allCategories');

        // when subscription is ready
        return {
            currentUser: Meteor.user(),
            profile: Artist.findOne({userId: Meteor.userId()}),
            categories: Category.find({isActive: true}, {sort: {title: 1}}).fetch(),
            defaultCategory: Session.get('defaultCategory')
        };
    },

    renderCategory() {
        return this.data.categories.map((category) => {
            return <div
                key={category._id}
                className="item"
                data-value={category.slug}
                data-text={category.title}>{category.title}</div>
        });
    },

    render() {
        return <Headroom>
            <div className="ui top attached secondary menu main-menu">
                <div className="item" style={{'margin': '0', 'padding': '0', 'fontSize': '1.3rem'}}>
                    <div className="ui dropdown logo" style={{'height': '57px', 'lineHeight': '57px', 'padding': '0 20px'}}>
                        <span style={{'position': 'relative', 'top': '4px', 'left': '-2px'}}>
                            <img src="/images/logo.png" />
                        </span>
                        midtube <small>{this.data.defaultCategory}</small>
                        <i className="dropdown icon"></i>
                        <div className="menu">
                            <div className="item" data-value=".com" data-text="All">All</div>
                            {this.renderCategory()}
                        </div>
                    </div>
                </div>
                <div className="item search-container">
                    <SearchForm />
                </div>

                <div className="right menu stackable">
                    <a className="item" href="/">
                        <i className="home icon"></i> Home
                    </a>
                    <a className="item" href="/albums">
                        <i className="list icon"></i> Music
                    </a>
                    { (this.data.currentUser && this.data.profile) ?
                        <TopMenuDropDown
                            currentUser={this.data.currentUser}
                            profile={this.data.profile} />
                        : '' }
                    { this.data.currentUser ?
                        <div className="item">
                            <a className="ui primary button" href="/album/add">
                                <i className="cloud upload icon"></i> Upload
                            </a>
                        </div>
                        : '' }
                    { !this.data.currentUser ?
                        <a className="item" href="/login">
                            <i className="sign in icon"></i> Login
                        </a>
                        : '' }
                </div>
            </div>
        </Headroom>
    }
});
