Footer = React.createClass({
    render() {
        return <div className="ui inverted vertical footer segment">
            <div className="ui center aligned container">
                <div className="ui grid page">
                    <div className="ui centered" style={{'width': '100%'}}>&copy; 2016 MidTube.com</div>
                    <div className="ui horizontal inverted small divided link list" style={{'width': '100%'}}>
                        <a className="item" href="https://news.midtube.com">News</a>
                        <a className="item" href="http://ui.midtube.com/page/why">Why MidTube?</a>
                        <a className="item" href="http://ui.midtube.com/page/about">About Us</a>
                        <a className="item" href="http://ui.midtube.com/page/faq">FAQ</a>
                        <a className="item" href="/contact">Contact</a>
                        <a className="item" href="https://www.facebook.com/MidTube-819036221461689/wbkd"><i className="facebook icon"></i></a>
                        <a className="item" href="https://twitter.com/midtube"><i className="twitter icon"></i></a>
                    </div>
                </div>
            </div>
        </div>
    }
});
