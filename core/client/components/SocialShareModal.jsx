SocialShareModal = React.createClass({
    propTypes: {
        showModal: React.PropTypes.bool.isRequired,
        hideModal: React.PropTypes.func.isRequired
    },

    componentDidUpdate() {
        $('#shareModal')
            .modal({detachable: false})
            .modal(this.props.showModal ? 'show' : 'hide');
    },

    render() {
        return <div className="ui small modal" id="shareModal">
            <div className="header">Share</div>
            <div className="content">
                <SocialShare />
            </div>
        </div>
    }
});
