TopMobileMenu = React.createClass({
    showSidebar() {
        $('.ui.sidebar').sidebar('toggle');
    },

    render() {
        return <Headroom>
            <div className="ui menu mobile">
                <a className="item logo" onClick={this.showSidebar}>
                    <i className="sidebar icon"></i>
                    midtube
                </a>
                <div className="right item" style={{'width': '60%'}}>
                    <SearchForm />
                </div>
            </div>
        </Headroom>
    }
});
