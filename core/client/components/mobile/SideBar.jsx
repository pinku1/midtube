SideBar = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    getMeteorData() {
        //get current user artist profile
        Meteor.subscribe('userProfile', null);

        // when subscription is ready
        return {
            currentUser: Meteor.user(),
            profile: Artist.findOne({userId: Meteor.userId()})
        };
    },

    logOut() {
        AccountsTemplates.logout();
    },

    profileUrl() {
        if (!_.isUndefined(this.data.profile)) {
            return FlowRouter.path('/profile/:slug', {slug: this.data.profile.slug});
        }
    },

    render() {
        return <div className="ui left vertical menu thin sidebar inverted labeled icon">
            <a className="item" href="/">
                <i className="home icon"></i>
                Home
            </a>
            <a className="item" href="/albums">
                <i className="sound icon"></i>
                Music
            </a>
            { !this.data.currentUser ?
                <a className="item" href="/login">
                    <i className="sign in icon"></i> Login
                </a>
            : '' }
            { this.data.currentUser ?
                <div>
                    <a className="item" href={this.profileUrl()}>
                        <i className="user icon"></i> Profile
                    </a>
                    <a className="item" href="/profile/update">
                        <i className="edit icon"></i> My Account
                    </a>
                    <a className="item" href={this.logOut}>
                        <i className="sign out icon"></i> Log Out
                    </a>
                </div>
            : '' }
        </div>
    }
});
