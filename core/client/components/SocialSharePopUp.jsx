SocialSharePopUp = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired
    },

    componentDidMount() {
        $('.share-btn')
            .popup({
                popup : $('.share.popup'),
                on    : 'click'
            })
        ;
    },

    render() {
        return <div>
            <button className="ui basic button share-btn">
                <i className="share green icon"></i>
                Share
            </button>
            <div className="ui fluid share popup top left transition hidden">
                <SocialShare title={this.props.title} />
            </div>
        </div>
    }
});
