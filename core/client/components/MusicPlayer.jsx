var emptySong = {
    title: 'no track loaded',
    file: {
        path: null
    }
};

MusicPlayerChild = React.createClass({
    propTypes: {
        playerStatus: React.PropTypes.string.isRequired,
        currentSong: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            position: 0,
            progress: 0
        };
    },

    play() {
        Session.set('playerStatus', Sound.status.PLAYING);
    },

    pause() {
        Session.set('playerStatus', Sound.status.PAUSED);
    },

    stop() {
        Session.set('playerStatus', Sound.status.STOPPED);
    },

    onPlaying(data) {
        //console.log(data);
        //console.log(data.position, data.duration);
        if (Session.get('playerStatus') === Sound.status.STOPPED) {
            this.setState({
                position: 0,
                progress: 0
            });
        } else {
            var percentage = (data.position / data.duration) * 100;
            this.setState({
                position: data.position,
                progress: parseInt(percentage)
            });
        }
    },

    finishedPlaying() {
        //reset position to zero
        this.setState({
            position: 0
        });
    },

    isPlaying() {
        return Session.get('playerStatus') === Sound.status.PLAYING;
    },

    getCurrentTrackUrl() {
        if (!_.isNull(this.props.currentSong.file.path)) {
            return this.props.currentSong.file.path;
        }

        return false;
    },

    render() {
        return (
            <div className="ui fluid card player">
                <div className="ui tiny progress green" data-percent={this.state.progress} style={{'borderRadius': '0 !important'}}>
                    <div className="bar" style={{'transitionDuration':'300ms', 'width': this.state.progress + '%'}}></div>
                </div>
                <div className="controls">
                    { this.isPlaying() ?
                        <button className="ui red icon button play-pause" onClick={this.pause}>
                            <i className="pause icon"></i>
                        </button>
                        :
                        <button className="ui red icon button play-pause" onClick={this.play}>
                            <i className="play icon"></i>
                        </button>
                    }
                    <div className="details">
                        <div className="header">{truncate(this.props.currentSong.title, 36)}</div>
                        {!_.isUndefined(this.props.currentSong.artists) ?
                            <div className="meta">by <ArtistsNames artists={this.props.currentSong.artists} /></div>
                        : '' }
                    </div>
                </div>
                { this.getCurrentTrackUrl() ?
                <Sound
                    url={this.getCurrentTrackUrl()}
                    playStatus={this.props.playerStatus}
                    onPlaying={this.onPlaying}
                    onFinishedPlaying={this.finishedPlaying}/>
                    : '' }
            </div>
        )
    }
});

MusicPlayer = React.createClass({
    mixins: [ReactMeteorData],

    getMeteorData() {
        return {
            playerStatus: Session.get('playerStatus'),
            currentSong: Session.get('playerSong')//_.isUndefined(Session.get('playerSong')) ? emptySong : Session.get('playerSong')
        };
    },

    componentWillMount() {
        Session.set('playerStatus', Sound.status.STOPPED);
        Session.set('playerSong', emptySong);
    },

    render() {
        return <MusicPlayerChild
            playerStatus={this.data.playerStatus || Sound.status.STOPPED}
            currentSong={this.data.currentSong || emptySong} />
    }
});
