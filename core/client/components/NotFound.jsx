NotFound = React.createClass({
    render() {
        return <div className="ui grid page main">
            <h2 className="ui header">Page you are looking for is not found.</h2>
        </div>
    }
});
