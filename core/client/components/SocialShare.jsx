//Ref: https://github.com/olahol/react-social/blob/master/react-social.js
var jsonp = function (url, cb) {
    var now = +new Date(),
        id = now + "_" + Math.floor(Math.random()*1000);

    var script = document.createElement("script"),
        callback = "jsonp_" + id,
        query = url.replace("@", callback);

    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", query);
    document.body.appendChild(script);

    window[callback] = cb;
};

var stripTrailingSlash = function (str) {
    if(str.substr(-1) === '/') {
        return str.substr(0, str.length - 1);
    }
    return str;
};

SocialShare = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired
    },

    getInitialState() {
        return {
            url: null,
            twitterCount: 0,
            facebookCount: 0
        };
    },

    componentDidMount() {
        var root = this;
        Tracker.autorun(function(c) {
            FlowRouter.watchPathChange();
            var currentContext = FlowRouter.current();

            root.setState({
                url: stripTrailingSlash(Meteor.absoluteUrl()) + currentContext.path
            });

            setTimeout(function () {
                root.getTwitterCount();
                root.getFacebookCount();

                c.stop();
            }, 1000);
        });
    },

    getTwitterCount() {
        var url = "https://cdn.api.twitter.com/1/urls/count.json?callback=@&url=" + encodeURIComponent(this.state.url);

        jsonp(url, function (data) {
            console.log(data);
            this.setState({
                twitterCount: data.count
            });
        }.bind(this));
    },

    //https://graph.facebook.com/v2.1/oauth/access_token?client_id=351944441565472&client_secret=905242f383530abb4f5882be27701c06&grant_type=client_credentials
    //351944441565472|VM3VplcRXZgauhFTzrbiJRrA7y8
    getFacebookCount() {
        //var fql = encodeURIComponent("select like_count, share_count from link_stat where url = '" + this.state.url + "'");
        //var url = "https://api.facebook.com/method/fql.query?format=json&callback=@&query=" + fql;
        var accessToken = '351944441565472|VM3VplcRXZgauhFTzrbiJRrA7y8';
        var url = 'https://graph.facebook.com/v2.1/?access_token=' + accessToken + '&id=' + encodeURIComponent(this.state.url);
        console.log(url);

        jsonp(url, function (data) {
            console.log(data);
            if (!_.isUndefined(data.share)) {
                this.setState({
                    facebookCount: data.share.share_count
                });
            }
        }.bind(this));
    },

    shareTwitter() {
        var url = "https://twitter.com/intent/tweet?text=" + this.props.title + ' at MidTube.com ' + encodeURIComponent(this.state.url);
        window.open(url, "_blank");
    },

    shareFacebook() {
        var url = "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(this.state.url);
        window.open(url, "_blank");
    },

    render() {
        return (
            <div style={{'width': '390px', 'paddingRight': '30px'}}>
                <div className="ui labeled button" tabIndex="0" onClick={this.shareTwitter}>
                    <div className="ui twitter button">
                        <i className="twitter icon"></i> Twitter
                    </div>
                    <a className="ui basic label">
                        {this.state.twitterCount || 0}
                    </a>
                </div>
                <div className="ui labeled button" tabIndex="0" onClick={this.shareFacebook}>
                    <div className="ui facebook button">
                        <i className="facebook icon"></i> Facebook
                    </div>
                    <a className="ui basic label">
                        {this.state.facebookCount || 0}
                    </a>
                </div>
                <div className="ui input" style={{'width': '100%', 'marginTop': '12px'}}>
                    <input type="text" value={this.state.url} readOnly="true" />
                </div>
            </div>
        )
    }
});
