LoginModal = React.createClass({
    componentDidMount() {
        setTimeout(function(){
            if (_.isUndefined(Cookie.get('hideLogin'))) {
                //if hide cookie is not set
                $('#loginModal')
                    .modal({detachable: false})
                    .modal(_.isNull(Meteor.userId()) ? 'show' : 'hide');
            }
        }, 1000);
    },

    hideModal() {
        //set cookie to dont show modal again for 2 days
        Cookie.set('hideLogin', 'true', {
            expires: 2
        });
    },

    render() {
        return (
            <div className="ui small modal" id="loginModal">
                <div className="content">
                    <UserAccountsWrapper />
                </div>
                <div className="actions">
                    <div className="ui black deny button" onClick={this.hideModal}>
                        Later
                    </div>
                </div>
            </div>
        )
    }
});
