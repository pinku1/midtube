Login = React.createClass({
    render() {
        return <div className="ui grid page main">
        	<UserAccountsWrapper />
            <div className="ui segment" style={{'textAlign': 'right', 'width': '97%', 'margin': '10px auto'}}>
                <p>Forgot password? <a href="/forget-password">click here</a></p>
            </div>
        </div>
    }
});
