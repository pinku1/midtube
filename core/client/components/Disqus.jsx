Disqus = React.createClass({
    componentDidMount() {
        /* * * CONFIGURATION VARIABLES * * */
        var disqus_shortname = 'midtube';

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    },

    render() {
        return <div id="disqus_thread" style={{'width': '100%'}}></div>
    }
});
