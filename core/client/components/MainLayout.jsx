MainLayout = React.createClass({
    mixins: [ScreenResizeMixin, ReactMeteorData],

    getMeteorData() {
        return {
            isMobile: Session.get('isMobile')
        }
    },

    getWidth(width) {
        if (width < 723) {
            Session.set('isMobile', true);
        } else {
            Session.set('isMobile', false);
        }
    },

    componentDidMount() {
        Session.setDefault('isMobile', this.state.isMobile);
    },

    mobileLayout() {
        return <div>
            <SideBar />
            <div className="dimmed pusher">
                <TopMobileMenu />
                <BertWrapper />
                {this.props.content}
                <Footer />
                <LoginModal />
                <HtmlTemplatesWrapper />
            </div>
        </div>
    },

    desktopLayout() {
        return <div>
            <TopMenu />
            <BertWrapper />
            {this.props.content}
            <Footer />
            <LoginModal />
            <HtmlTemplatesWrapper />
        </div>
    },

    render() {
        return this.data.isMobile ? this.mobileLayout() : this.desktopLayout();
    }
});
