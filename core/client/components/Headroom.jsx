var shouldUpdate = function(lastKnownScrollY, currentScrollY, props, state) {
    var distanceScrolled, ref, ref1, ref2, scrollDirection;
    if (lastKnownScrollY == null) {
        lastKnownScrollY = 0;
    }
    if (currentScrollY == null) {
        currentScrollY = 0;
    }
    if (props == null) {
        props = {};
    }
    if (state == null) {
        state = {};
    }
    scrollDirection = currentScrollY >= lastKnownScrollY ? "down" : "up";
    distanceScrolled = Math.abs(currentScrollY - lastKnownScrollY);
    if (currentScrollY <= 0 && state.state !== "unfixed") {
        return {
            action: "unfix",
            scrollDirection: scrollDirection,
            distanceScrolled: distanceScrolled
        };
    } else if (currentScrollY <= state.height && scrollDirection === "down" && state.state === "unfixed") {
        return {
            action: "none",
            scrollDirection: scrollDirection,
            distanceScrolled: distanceScrolled
        };
    } else if (scrollDirection === "down" && ((ref = state.state) === "pinned" || ref === "unfixed") && currentScrollY > state.height && distanceScrolled > props.downTolerance) {
        return {
            action: "unpin",
            scrollDirection: scrollDirection,
            distanceScrolled: distanceScrolled
        };
    } else if (scrollDirection === "up" && distanceScrolled > props.upTolerance && ((ref1 = state.state) !== "pinned" && ref1 !== "unfixed")) {
        return {
            action: "pin",
            scrollDirection: scrollDirection,
            distanceScrolled: distanceScrolled
        };
    } else if (scrollDirection === "up" && currentScrollY <= state.height && ((ref2 = state.state) !== "pinned" && ref2 !== "unfixed")) {
        return {
            action: "pin",
            scrollDirection: scrollDirection,
            distanceScrolled: distanceScrolled
        };
    } else {
        return {
            action: "none",
            scrollDirection: scrollDirection,
            distanceScrolled: distanceScrolled
        };
    }
};

Headroom = React.createClass({
    mixins: [PureRenderMixin],

    currentScrollY: 0,
    lastKnownScrollY: 0,
    ticking: false,

    propTypes: {
        children: React.PropTypes.any.isRequired,
        disableInlineStyles: React.PropTypes.bool,
        disable: React.PropTypes.bool,
        upTolerance: React.PropTypes.number,
        downTolerance: React.PropTypes.number,
        onPin: React.PropTypes.func,
        onUnpin: React.PropTypes.func,
        onUnfix: React.PropTypes.func,
        wrapperStyle: React.PropTypes.object
    },

    getDefaultProps() {
        return {
            disableInlineStyles: false,
            disable: false,
            upTolerance: 5,
            downTolerance: 0,
            onPin: function() {},
            onUnpin: function() {},
            onUnfix: function() {},
            wrapperStyle: {}
        };
    },

    getInitialState() {
        return {
            state: 'unfixed',
            translateY: 0,
            className: 'headroom headroom--pinned'
        };
    },

    componentDidMount() {
        this.setState({
            height: ReactDOM.findDOMNode(this.refs.inner).offsetHeight
        });
        if (!this.props.disable) {
            return window.addEventListener('scroll', this.handleScroll);
        }
    },

    componentWillUnmount() {
        return window.removeEventListener('scroll', this.handleScroll);
    },

    componentWillReceiveProps(nextProps) {
        if (nextProps.disable && !this.props.disable) {
            this.unfix();
            return window.removeEventListener('scroll', this.handleScroll);
        } else if (!nextProps.disable && this.props.disable) {
            return window.addEventListener('scroll', this.handleScroll);
        }
    },

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.children !== this.props.children) {
            return this.setState({
                height: ReactDOM.findDOMNode(this.refs.inner).offsetHeight
            });
        }
    },

    handleScroll() {
        if (!this.ticking) {
            this.ticking = true;
            return raf(this.update);
        }
    },

    unpin() {
        this.props.onUnpin();
        return this.setState({
            translateY: "-100%",
            className: "headroom headroom--unpinned"
        }, (function(_this) {
            return function() {
                return setTimeout((function() {
                    return _this.setState({
                        state: "unpinned"
                    });
                }), 0);
            };
        })(this));
    },

    pin() {
        this.props.onPin();
        return this.setState({
            translateY: 0,
            className: "headroom headroom--pinned",
            state: "pinned"
        });
    },

    unfix() {
        this.props.onUnfix();
        return this.setState({
            translateY: 0,
            className: "headroom headroom--unfixed",
            state: "unfixed"
        });
    },

    update() {
        var action, distanceScrolled, ref, scrollDirection;
        this.currentScrollY = this.getScrollY();
        ref = shouldUpdate(this.lastKnownScrollY, this.currentScrollY, this.props, this.state), action = ref.action, scrollDirection = ref.scrollDirection, distanceScrolled = ref.distanceScrolled;
        if (action === "pin") {
            this.pin();
        } else if (action === "unpin") {
            this.unpin();
        } else if (action === "unfix") {
            this.unfix();
        }
        this.lastKnownScrollY = this.currentScrollY;
        return this.ticking = false;
    },

    getScrollY() {
        if (window.pageYOffset !== void 0) {
            return window.pageYOffset;
        } else if (window.scrollTop !== void 0) {
            return window.scrollTop;
        } else {
            return (document.documentElement || document.body.parentNode || document.body).scrollTop;
        }
    },

    render() {
        var style, wrapperStyles;

        style = {
            position: this.props.disable || this.state.state === "unfixed" ? 'relative' : 'fixed',
            top: 0,
            left: 0,
            right: 0,
            zIndex: 1,
            WebkitTransform: "translateY(" + this.state.translateY + ")",
            MsTransform: "translateY(" + this.state.translateY + ")",
            transform: "translateY(" + this.state.translateY + ")"
        };

        if (this.state.state !== "unfixed") {
            style = _.extend(style, {
                WebkitTransition: "all .2s ease-in-out",
                MozTransition: "all .2s ease-in-out",
                OTransition: "all .2s ease-in-out",
                transition: "all .2s ease-in-out"
            });
        }

        if (!this.props.disableInlineStyles) {
            style = _.extend(style, this.props.style);
        } else {
            style = this.props.style;
        }

        //wrapperStyles = _.extend(this.props.wrapperStyle, {
        //    height: !_.isUndefined(this.state.height) ? this.state.height : void 0
        //});
        //todo this was quick fix
        wrapperStyles = {
            height: !_.isUndefined(this.state.height) ? this.state.height : 59
        };

        return <div style={wrapperStyles} className="headroom-wrapper">
            <div ref="inner" {...this.props} style={style} className={this.state.className}>
                {this.props.children}
            </div>
        </div>
    }
});
