PageHeader = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired,
        moreText: React.PropTypes.string,
        moreLink: React.PropTypes.string
    },

    render() {
        return (
            <div className="ui grid container" style={{'marginBottom': '18px'}}>
                <div className="ui clearing" style={{'width': '100%'}}>
                    <h2 className="ui left floated header" style={{'fontWeight': 600, fontSize: '1.8rem'}}>
                        {this.props.title}
                    </h2>
                    {!_.isUndefined(this.props.moreText) ?
                    <a className="ui right floated header" href={this.props.moreLink} style={{'color': '#ff4020', 'marginTop': '8px'}}>
                        {this.props.moreText} <i className="long arrow right icon" style={{'float': 'right', 'marginLeft': '6px', 'marginTop': '-3px'}}></i>
                    </a>:''}
                </div>
            </div>
        )
    }
});
