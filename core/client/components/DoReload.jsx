DoReload = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        currentPage: React.PropTypes.string.isRequired
    },

    getInitialState() {
        return {
            currentPage: null
        };
    },

    doSmoothScroll() {
        smoothScroll.animateScroll( null, '#react-root' );
    },

    componentDidMount() {
        this.doSmoothScroll();

        if (_.isNull(this.state.currentPage)) {
            this.setState({
                currentPage: this.props.currentPage
            });
        }
    },

    componentDidUpdate() {
        var oldState = this.state.currentPage;

        if (!_.isNull(oldState) && oldState !== this.props.currentPage) {
            this.setState({
                currentPage: this.props.currentPage
            });

            console.log('reload now! page changed');
            FlowRouter.reload();
            this.doSmoothScroll();
        }
    },

    render() {
        return <div></div>
    }
});
