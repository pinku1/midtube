Restricted = React.createClass({
    render() {
        return <div className="ui grid page">
            <h2 className="ui header">Not allowed to view this area, please login.</h2>
        </div>
    }
});
