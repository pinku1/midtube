FlowRouter.route('/not-found', {
    name: 'notFound',
    action: function() {
        ReactLayout.render(MainLayout, {
            content: <NotFound />
        });
    }
});
