ScreenResizeMixin = {
    getInitialState: function() {
        return {windowWidth: window.innerWidth};
    },

    handleResize: function(e) {
        this.setState({windowWidth: window.innerWidth});

        this.getWidth(this.state.windowWidth);
    },

    componentDidMount: function() {
        this.getWidth(this.state.windowWidth);

        window.addEventListener('resize', this.handleResize);
    },

    componentWillUnmount: function() {
        window.removeEventListener('resize', this.handleResize);
    }
};
