if (Meteor.App) {
    throw new Meteor.Error('Meteor.App already defined? see client/lib/constants.js');
}

Meteor.App = {
    NAME: 'MidTube',
    DESCRIPTION: 'MidTube Official Website'
};
