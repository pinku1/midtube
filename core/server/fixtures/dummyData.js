// The "||" notation doesn't work yet
Fixtures = typeof Fixtures !== "undefined" ? Fixtures : {};

Fixtures.users = [
    {
        "_id": "56098f9eed01840dc38fc2dd",
        "emails" : [{"address" : "perminder.klair@gmail.com", "verified" : true}], 
        "services" : { "password" : { "bcrypt" : "$2a$10$CCzzYL/1ZRbhytUM3aiMeOCiGx9XXmRZ1kiUyfB0FvfPWbh2hLcmG" }},
        "resume" : { "loginTokens" : [{"when" : 'ISODate("2015-02-26T15:28:51.272Z")', "hashedToken" : "JUjmXp4Q4gUMIJ/cXZ+3uVhWzADHp5NKzRG9ONV7zd8=" }]}
    }
];

Fixtures.artists = [
  { 'title': 'Singer', 'fullName' : 'Parminder Klair', 'slug' : 'klair', 'biography' : Fake.sentence(), 'featured' : 1, 'userId' : '56098f9eed01840dc38fc2dd' },
  { 'title': 'Singer', 'fullName' : 'Jazzy B', 'slug' : 'jazzy-b', 'biography' : Fake.sentence(), 'featured' : 1 },
  { 'title': 'Singer', 'fullName' : 'Gippy Grewal', 'slug' : 'gippy-grewal', 'biography' : Fake.sentence(), 'featured' : 1 },
  { 'title': 'Singer', 'fullName' : 'Honey Singh', 'slug' : 'honey-singh', 'biography' : Fake.sentence(), 'featured' : 1 },
  { 'title': 'Singer', 'fullName' : 'Diljit', 'slug' : 'diljit', 'biography' : Fake.sentence(), 'featured' : 1 }
];

var defaultCategory = {_id: 'SuBDdmvCTYP7peamQ', title: 'Punjabi', slug: 'punjabi'};

Fixtures.albums = [
  { '_id': '56098f9eed01840dc38fc4df', 'title' : 'Feem', artists: [{fullName : Fake.word()}], 'slug' : 'feem', 'description' : Fake.sentence(), 'featured' : 1, 'owner_id': '56098f9eed01840dc38fc2dd', category: defaultCategory },
  { 'title' : 'All Eyez on me', artists: [{fullName : Fake.word()}], 'slug' : 'all-eyez-on-me', 'description' : Fake.sentence(), 'featured' : 1, category: defaultCategory },
  { 'title' : 'Back to Basics', artists: [{fullName : Fake.word()}], 'slug' : 'back-to-basics', 'description' : Fake.sentence(), 'featured' : 1, category: defaultCategory },
  { 'title' : 'The Next Level', artists: [{fullName : Fake.word()}], 'slug' : 'the-next-level', 'description' : Fake.sentence(), 'featured' : 1, category: defaultCategory },
  { 'title' : 'Desi Kalakaar', artists: [{fullName : Fake.word()}], 'slug' : 'desi-kalakaar', 'description' : Fake.sentence(), 'featured' : 1, category: defaultCategory }
];

var date = new Date();

defaultCategory = {_id: 'hrXTnEyqBk9DENW4A', title: 'Bollywood', slug: 'bollywood'};
for (var i = 0; i < 8; i++) {
    Fixtures.albums.push({ 'title' : Fake.word(), artists: [{fullName : Fake.word()}], 'slug' : Fake.word(), 'description' : Fake.sentence(), created: date, 'featured' : 1, category: defaultCategory });
}

defaultCategory = {_id: 'eJqozmanyG4oTD7DS', title: 'Pop', slug: 'pop'};
for (var j = 0; j < 15; j++) {
    Fixtures.albums.push({ 'title' : Fake.word(), artists: [{fullName : Fake.word()}], 'slug' : Fake.word(), 'description' : Fake.sentence(), created: date, category: defaultCategory });
}

Fixtures.songs = [
  { 'title' : 'Kharku', 'slug' : 'kharku', 'description' : Fake.sentence(), 'featured' : 1, 'albumId': '56098f9eed01840dc38fc4df' },
  { 'title' : 'Truck', 'slug' : 'truck', 'description' : Fake.sentence(), 'featured' : 0, 'albumId': '56098f9eed01840dc38fc4df' },
  { 'title' : 'Strawberry', 'slug' : 'strawberry', 'description' : Fake.sentence(), 'featured' : 0, 'albumId': '56098f9eed01840dc38fc4df' },
  { 'title' : 'Chunni', 'slug' : 'chunni', 'description' : Fake.sentence(), 'featured' : 1, 'albumId': '56098f9eed01840dc38fc4df' },
  { 'title' : 'Band Bottle', 'slug' : 'band-bottle', 'description' : Fake.sentence(), 'featured' : 0, 'albumId': '56098f9eed01840dc38fc4df' }
];

Fixtures.paymentPlan = [
    {
        title: 'iTunes, Google Play, Youtube, Spotify, Amazon Music Distribution',
        description: 'You album will be easily distributed to various networks including iTunes, Google Play, Youtube, Spotify, Amazon Music, Saavn, Deezer. ' +
        'You keep 100% of your rights and 100% of the money you earn.',
        planPrice: '7'
    },
    {
        title: 'Twitter, Instagram, Facebook Page Social Share',
        description: 'Your album will be shared on various social networks including Twitter, Instagram, Facebook Page Share.',
        planPrice: '0'
    },
    {
        title: 'Homepage video play package',
        description: 'Your music video be played on Midtube TV player.',
        planPrice: '0'
    },
    {
        title: 'Featured Album',
        description: 'Album will be listed and promoted on homepage of Midtube under featured section.',
        planPrice: '0'
    }
];

Fixtures.categories = [
    {title: 'Hip Hop', slug: 'hip-hop'},
    {title: 'Classic', slug: 'classic'},
    {_id: 'SuBDdmvCTYP7peamQ', title: 'Punjabi', slug: 'punjabi'},
    {_id: 'hrXTnEyqBk9DENW4A', title: 'Bollywood', slug: 'bollywood'},
    {_id: 'eJqozmanyG4oTD7DS', title: 'Pop', slug: 'pop'},
    {title: 'Romance', slug: 'romance'}
];
