PrettyEmail.options = {
    from: 'admin@midtube.com',
    logoUrl: 'http://mycompany.com/logo.png',
    companyName: 'MidTube',
    companyUrl: 'http://midtube.com',
    companyAddress: '123 Street, ZipCode, City, Country',
    companyTelephone: '+1234567890',
    companyEmail: 'admin@midtube.com',
    siteName: 'MidTube.com'
};
