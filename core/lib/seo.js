SEO = new FlowRouterSEO();

SEO.setDefaults({
  title: 'MidTube',
  description: 'MidTube, a music marketplace',
  meta: {
    'property="og:type"': 'website',
    'property="og:site_name"': 'MidTube',
    'name="twitter:card"': 'MidTube, a music marketplace',
    'name="twitter:site"': '@pinku1'
  }
});
