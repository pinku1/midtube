loggedInOnly = function(context, redirect) {
    if (_.isNull(Meteor.user())) {
        redirect('/login');
    }
};

FlowRouter.triggers.enter([loggedInOnly], {
    only: ["albumAdd", "albumAddPicture", "albumUpdate", "albumSongsUpdate", "albumLinksUpdate",
        "albumExtrasUpdate", "profileUpdate", "profileMusic"]
});

FlowRouter.notFound = {
    action: function() {
        ReactLayout.render(MainLayout, {
            content: <NotFound />
        });
    }
};

FlowRouter.route('/restricted', {
    name: 'restricted',
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <Restricted />
        });
    }
});

FlowRouter.route('/login', {
    name: 'login',
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <Login />
        });
    }
});

FlowRouter.route('/forget-password', {
    name: 'forgetPassword',
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <ForgetPassword />
        });
    }
});
