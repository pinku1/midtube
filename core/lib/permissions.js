can = {};

// Permissions

can.post = function (user, returnError) {
    user = (typeof user === 'undefined') ? Meteor.user() : user;

    if (!user) {
        return returnError ? "no_account" : false;
    } else {
        return true;
    }
};
can.comment = function (user, returnError) {
    return can.post(user, returnError);
};
can.vote = function (user, returnError) {
    return can.post(user, returnError);
};
can.favorite = function (user, returnError) {
    return can.post(user, returnError);
};
can.subscribe = function (user, returnError) {
    return can.post(user, returnError);
};
can.edit = function (user, item, returnError) {
    user = (typeof user === 'undefined') ? Meteor.user() : user;

    if (!user || !item || (user._id !== item.userId)) {
        return returnError ? "no_rights" : false;
    } else {
        return true;
    }
};
can.editById = function (userId, item) {
    var user = Meteor.users.findOne(userId);
    return can.edit(user, item);
};
can.currentUserEdit = function (item) {
    return can.edit(Meteor.user(), item);
};
