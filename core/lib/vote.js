// returns how much "power" a user's votes have
var getVotePower = function () {
    return 1;
};

var modifyKarma = function (userId, karma) {
    Meteor.users.update({_id: userId}, {$inc: {karma: karma}});
};

var hasUpvotedItem = function (item, user) {
    return item.upVoters && item.upVoters.indexOf(user._id) != -1;
};

var hasFavoritedItem = function (item, user) {
    return item.favoriteBy && item.favoriteBy.indexOf(user._id) != -1;
};

var hasSubscribedItem = function (item, user) {
    return item.subscribedBy && item.subscribedBy.indexOf(user._id) != -1;
};

var addVote = function (userId, vote, collection, upOrDown) {
    var field = 'votes.' + upOrDown + 'voted' + collection;
    var add = {};
    add[field] = vote;
    var result = Meteor.users.update({_id: userId}, {
        $addToSet: add
    });
};

var removeVote = function (userId, itemId, collection, upOrDown) {
    var field = 'votes.' + upOrDown + 'voted' + collection;
    var remove = {};
    remove[field] = {itemId: itemId};
    Meteor.users.update({_id: userId}, {
        $pull: remove
    });
};

var addFavorite = function (userId, favorite, collection) {
    var field = 'favorited' + collection;
    var add = {};
    add[field] = favorite;
    var result = Meteor.users.update({_id: userId}, {
        $addToSet: add
    });
};

var removeFavorite = function (userId, itemId, collection, upOrDown) {
    var field = 'favorited' + collection;
    var remove = {};
    remove[field] = {itemId: itemId};
    Meteor.users.update({_id: userId}, {
        $pull: remove
    });
};

var addSubscription = function (userId, favorite, collection) {
    var field = 'subscribed' + collection;
    var add = {};
    add[field] = favorite;
    var result = Meteor.users.update({_id: userId}, {
        $addToSet: add
    });
};

var removeSubscription = function (userId, itemId, collection) {
    var field = 'subscribed' + collection;
    var remove = {};
    remove[field] = {itemId: itemId};
    Meteor.users.update({_id: userId}, {
        $pull: remove
    });
};

upvoteItem = function (collection, item, user) {
    user = typeof user === "undefined" ? Meteor.user() : user,
        votePower = getVotePower(),
        collectionName = collection._name.slice(0,1).toUpperCase() + collection._name.slice(1);

    // make sure user has rights to upvote first
    if (!user || !can.vote(user, true)) {
        return false;
    }

    // in case user is upvoting a previously upvote
    if (hasUpvotedItem(item, user)) {
        cancelUpvote(collection, item, user);
        return;
    }

    // Votes & Score
    var result = collection.update({_id: item && item._id, upVoters: { $ne: user._id }}, {
        $addToSet: {upVoters: user._id},
        $inc: {upVotes: 1, baseScore: votePower}
    });

    if (result > 0) {
        // Add item to list of upvoted items
        var vote = {
            itemId: item._id,
            votedAt: new Date(),
            power: votePower
        };
        addVote(user._id, vote, collectionName, 'up');

        // extend item with baseScore to help calculate newScore
        item = _.extend(item, {baseScore: (item.baseScore + votePower)});
        updateScore({collection: collection, item: item, forceUpdate: true});

        // if the item is being upvoted by its own author, don't give karma
        if (item.userId != user._id) {
            modifyKarma(item.userId, votePower);
        }
        
        // give karma to all previous upvoters of the post
        // (but not to the person doing the upvoting)
        _.each(item.upVoters, function (upvoterId) {
            // share the karma equally among all upvoters, but cap the value at 0.1
            var karmaIncrease = Math.min(0.1, votePower/item.upVoters.length);
            modifyKarma(upvoterId, 0.1);
        });

        // --------------------- Server-Side Async Callbacks --------------------- //

        //if (Meteor.isServer) {
        //    Meteor.defer(function () { // use defer to avoid holding up client
        //        // run all post submit server callbacks on post object successively
        //        result = upvoteCallbacks.reduce(function(result, currentFunction) {
        //            return currentFunction(collection, result, user);
        //        }, item);
        //    });
        //}

    }

    return true;
};

cancelUpvote = function (collection, item, user) {
    user = typeof user === "undefined" ? Meteor.user() : user,
        votePower = getVotePower(),
        collectionName = collection._name.slice(0,1).toUpperCase()+collection._name.slice(1);

    // if user isn't among the upvoters, abort
    if (!hasUpvotedItem(item, user)) {
        return false;
    }

    // Votes & Score
    var result = collection.update({_id: item && item._id, upVoters: user._id},{
        $pull: {upVoters: user._id},
        $inc: {upVotes: -1, baseScore: -votePower}
    });

    if (result > 0) {
        // Remove item from list of upvoted items
        removeVote(user._id, item._id, collectionName, 'up');

        // extend item with baseScore to help calculate newScore
        item = _.extend(item, {baseScore: (item.baseScore + votePower)});
        updateScore({collection: collection, item: item, forceUpdate: true});

        //if the item is being upvoted by its own author, don't give karma
        if (item.userId != user._id) {
            modifyKarma(item.userId, votePower);
        }
    }

    return true;
};

var favoriteItem = function (collection, item, user) {
    //cl(collection);
    user = typeof user === "undefined" ? Meteor.user() : user,
        votePower = getVotePower(),
        collectionName = collection._name.slice(0,1).toUpperCase()+collection._name.slice(1);

    // make sure user has rights to favorite first
    if (!user || !can.favorite(user, true)) {
        return false;
    }

    //if user already favorited, then unfavorite
    if (hasFavoritedItem(item, user)) {
        unfavoriteItem(collection, item, user);
        return;
    }

    // Votes & Score
    var result = collection.update({_id: item && item._id, favoriteBy: { $ne: user._id }}, {
        $addToSet: {favoriteBy: user._id},
        $inc: {favorites: 1}
    });

    if (result > 0) {
        // Add item to list of favorited items
        var favorite = {
            itemId: item._id,
            favoritedAt: new Date()
        };
        addFavorite(user._id, favorite, collectionName, 'up');

        // extend item with baseScore to help calculate newScore
        item = _.extend(item, {baseScore: (item.baseScore + votePower)});
        updateScore({collection: collection, item: item, forceUpdate: true});

    }

    return true;
};

var unfavoriteItem = function (collection, item, user) {
    user = typeof user === "undefined" ? Meteor.user() : user,
        collectionName = collection._name.slice(0,1).toUpperCase()+collection._name.slice(1);

    // if user isn't among the favorite by, abort
    if (!hasFavoritedItem(item, user)) {
        return false;
    }

    // unfavorite
    var result = collection.update({_id: item && item._id, favoriteBy: user._id},{
        $pull: {favoriteBy: user._id},
        $inc: {favorites: -1}
    });

    if (result > 0) {
        // Remove item from list of favorites items
        removeFavorite(user._id, item._id, collectionName, 'up');
    }
    
    return true;
};

var subscribeItem = function (collection, item, user) {
    user = typeof user === "undefined" ? Meteor.user() : user,
        votePower = getVotePower(),
        collectionName = collection._name.slice(0,1).toUpperCase()+collection._name.slice(1);

    // make sure user has rights to subscribe first
    if (!user || !can.subscribe(user, true)) {
        return false;
    }

    //if user already subscribed, then unsubscribe
    if (hasSubscribedItem(item, user)) {
        unSubscribeItem(collection, item, user);
        return;
    }

    // Subscribe & Score
    var result = collection.update({_id: item && item._id, subscribedBy: { $ne: user._id }}, {
        $addToSet: {subscribedBy: user._id},
        $inc: {subscribers: 1}
    });

    if (result > 0) {
        // Add item to list of subscribed items
        var subscribe = {
            itemId: item._id,
            favoritedAt: new Date()
        };
        addSubscription(user._id, subscribe, collectionName, 'up');

        // extend item with baseScore to help calculate newScore
        item = _.extend(item, {baseScore: (item.baseScore + votePower)});
        updateScore({collection: collection, item: item, forceUpdate: true});

    }

    return true;
};

var unSubscribeItem = function (collection, item, user) {
    user = typeof user === "undefined" ? Meteor.user() : user,
        collectionName = collection._name.slice(0,1).toUpperCase()+collection._name.slice(1);

    // if user isn't among the subscribe by, abort
    if (!hasSubscribedItem(item, user)) {
        return false;
    }

    // unsubscribe
    var result = collection.update({_id: item && item._id, subscribedBy: user._id},{
        $pull: {subscribedBy: user._id},
        $inc: {subscribers: -1}
    });

    if (result > 0) {
        // Remove item from list of subscribers items
        removeSubscription(user._id, item._id, collectionName, 'up');
    }

    return true;
};

var getUser = function (user) {
    // only let admins specify different users for voting
    // if no user is specified, use current user by default
    return (typeof user !== 'undefined') ? user : Meteor.user();
};

Meteor.methods({
    upvoteAlbum: function (album) {
        return upvoteItem.call(this, Albums, album);
    },
    favoriteAlbum: function (album) {
        return favoriteItem.call(this, Albums, album);
    },
    subscribeArtist: function (artist) {
        return subscribeItem.call(this, Artists, artist);
    }
});
