//https://github.com/meteor-useraccounts/core/blob/master/Guide.md
AccountsTemplates.configure({
    // Behavior
    confirmPassword: true,
    enablePasswordChange: true,
    forbidClientAccountCreation: false,
    overrideLoginErrors: true,
    sendVerificationEmail: false,
    lowercaseUsername: false,
    focusFirstInput: true,

    // Appearance
    showAddRemoveServices: false,
    showForgotPasswordLink: false,
    showLabels: true,
    showPlaceholders: true,
    showResendVerificationEmailLink: false,

    // Client-side Validation
    continuousValidation: false,
    negativeFeedback: false,
    negativeValidation: true,
    positiveValidation: true,
    positiveFeedback: true,
    showValidating: true,

    // Privacy Policy and Terms of Use
    privacyUrl: 'privacy',
    termsUrl: 'terms-of-use',

    // Redirects
    homeRoutePath: '/',
    redirectTimeout: 4000,

    // Hooks
    onLogoutHook: function () {
      FlowRouter.go('/');
    },
    onSubmitHook: function (error, state) {
        if (!error) {
            console.log(state);
            if (state === "signIn") {
                //check if in case user does't have artist profile
                Meteor.call('createArtist');

                //on login success, redirect to homepage
                FlowRouter.go('/');
            }

            //on signup create artist profile for user
            if (state === "signUp") {
                Meteor.call('createArtist', function (err, res) {
                    if (!_.isNull(res)) {
                        //if artist created, then redirect to profile update page
                        FlowRouter.go('/profile/update');
                        FlashMessages.sendSuccess('Registered successfully! Please update your profile now.');
                    }
                });
            }

            var loginModal = $('#loginModal');
            if (loginModal.length != 0) {
                loginModal
                    .modal({detachable: false})
                    .modal('hide');
            }

        }
    },
    //preSignUpHook: function () {},

    // Texts
    texts: {
      button: {
          signUp: "Register Now!"
      },
      socialSignUp: "Register",
      socialIcons: {
          "meteor-developer": "fa fa-rocket"
      },
      title: {
          forgotPwd: "Recover Your Password"
      }
    }
});

//turn off astro warnings
Astro.config.verbose = false;

AppSettings = {
    limit: 18
};
