FlowRouter.route('/packages', {
    name: 'packagesList',
    subscriptions: function(params) {
        this.register('allPaymentPlans', Meteor.subscribe('allPaymentPlans'));
    },
    action: function() {
        ReactLayout.render(MainLayout, {
            content: <PackagesList />
        });
    }
});
