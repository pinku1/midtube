var requiredString = Validators.and([
    Validators.required(),
    Validators.string(),
    Validators.minLength(3),
    Validators.maxLength(40)
]);
var requiredDate = Validators.and([
    Validators.required(),
    Validators.date()
]);

PaymentPlans = new Mongo.Collection('payment-plans');

PaymentPlan = Astro.Class({
    name: 'PaymentPlan',
    collection: PaymentPlans,
    fields: {
        title: {
            type: 'string',
            validator: requiredString
        },
        description: 'string',
        planPrice: {
            type: 'number',
            validator: Validators.number(),
            default: 0
        },
        type: { //once, monthly, yearly
            type: 'string',
            default: 'once'
        },
        isActive: {
            type: 'boolean',
            default: true
        }
    },
    behaviors: {
        timestamp: {
            hasCreatedField: true,
            createdFieldName: 'createdAt',
            hasUpdatedField: true,
            updatedFieldName: 'updatedAt'
        }
    }
});
