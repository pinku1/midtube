var requiredString = Validators.and([
    Validators.required(),
    Validators.string(),
    Validators.minLength(3),
    Validators.maxLength(40)
]);
var requiredDate = Validators.and([
    Validators.required(),
    Validators.date()
]);

Orders = new Mongo.Collection('orders');

Order = Astro.Class({
    name: 'Order',
    collection: Orders,
    fields: {
        stripePaymentId: 'string',
        userId: 'string',
        albumId: 'string',
        songId: 'string',
        amount: {
            type: 'number',
            validator: Validators.number(),
            default: 0
        },
        status: {
            type: 'string',
            default: 'pending'
        }
    },
    behaviors: {
        timestamp: {
            hasCreatedField: true,
            createdFieldName: 'createdAt',
            hasUpdatedField: true,
            updatedFieldName: 'updatedAt'
        }
    }
});
