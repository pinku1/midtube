var createStripeCharge = function (token, amount, callback) {
    StripeServer.charges.create({
        amount: amount,
        currency: 'usd',
        source: token
    }, Meteor.bindEnvironment(function (err, result) {
        //http://stackoverflow.com/questions/27769527/error-meteor-code-must-always-run-within-a-fiber
        //console.log(err, result);

        callback(err, result);
    }));
};

var orderAlbumWrap = Async.wrap(function (stripeToken, albumId, userId, callbackMain) {
    var album = {};
    var charge = {};
    var user = {};

    console.log('wrapp async!!!');
    async.series([
            function (callback) {
                user = Meteor.users.findOne({_id: userId});

                if (!_.isUndefined(user.emails)) {
                    callback(null, user);
                } else {
                    callback('Please set your email address first.', false);
                }
            },
            function (callback) {
                //get album
                album = Album.findOne(albumId);
                if (_.isUndefined(album)) {
                    callback('album-not-found', null);
                } else {
                    callback(null, album);
                }
            },
            function (callback) {
                //create order on stripe
                var cost = album.sales.price.toFixed(2);
                var stripeCost = cost.replace(/\./g, "");
                createStripeCharge(stripeToken, stripeCost, function (err, chargeResult) {
                    charge = chargeResult;
                    callback(err, chargeResult)
                });
            },
            function (callback) {
                //create order record on local
                var order = new Order();
                order.set({
                    stripePaymentId: charge.id,
                    userId: userId,
                    albumId: album._id,
                    amount: album.sales.price,
                    status: charge.status
                });
                order.save();

                callback(null, order);
            },
            function (callback) {
                //todo mark album bought for user ? do i need? or user orders table?

                //charge.source.id //todo store this is card ID for future payments
                callback(null, true);
            },
            function (callback) {
                //send invoice and download link
                PrettyEmail.send('call-to-action', {
                    from: 'admin@midtube.com',
                    to: user.emails[0].address,
                    subject: 'Your purchase at MidTube.com',
                    heading: 'Your order at Midtube.com',
                    headingSmall: 'Thank you very much for your order! If any question or issues feel free to reply this email.',
                    message: 'Click the button below to start downloading your purchased music item.',
                    buttonText: 'Download',
                    buttonUrl: album.file.zip
                });

                callback(null, true);
            }
        ],
        function (err, results) {
            // results is now equal to ['one', 'two']
            //console.log(err, results);
            console.log('sync done!');
            callbackMain(err, results[2]);
        });
});

var orderSongWrap = Async.wrap(function (stripeToken, albumId, songId, userId, callbackMain) {
    var album = {};
    var song = {};
    var charge = {};
    var user = {};

    async.series([
            function (callback) {
                user = Meteor.users.findOne({_id: userId}, {fields: {emails: 1, profile: 1}});

                if (!_.isUndefined(user.emails)) {
                    callback(null, user);
                } else {
                    callback('Please set your email address first.', false);
                }
            },
            function (callback) {
                //get album
                album = Album.findOne(albumId);
                if (_.isUndefined(album)) {
                    callback('album-not-found', null);
                } else {
                    callback(null, album);
                }
            },
            function (callback) {
                //get song
                song = Song.findOne(songId);
                if (_.isUndefined(song)) {
                    callback('song-not-found', null);
                } else {
                    callback(null, song);
                }
            },
            function (callback) {
                //create order on stripe
                var cost = album.sales.songPrice.toFixed(2);
                var stripeCost = cost.replace(/\./g, "");
                createStripeCharge(stripeToken, stripeCost, function (err, chargeResult) {
                    charge = chargeResult;
                    callback(err, chargeResult)
                });
            },
            function (callback) {
                //create order record on local
                var order = new Order();
                order.set({
                    stripePaymentId: charge.id,
                    userId: userId,
                    albumId: album._id,
                    songId: song._id,
                    amount: album.sales.songPrice,
                    status: charge.status
                });
                order.save();

                callback(null, order);
            },
            function (callback) {
                //todo mark album bought for user ? do i need? or user orders table?

                //charge.source.id //todo store this is card ID for future payments
                callback(null, true);
            },
            function (callback) {
                //send invoice and download link
                PrettyEmail.send('call-to-action', {
                    from: 'admin@midtube.com',
                    to: user.emails[0].address,
                    subject: 'Your purchase at MidTube.com',
                    heading: 'Your order at Midtube.com',
                    headingSmall: 'Thank you very much for your order! If any question or issues feel free to reply this email.',
                    message: 'Click the button below to start downloading your purchased music item.',
                    buttonText: 'Download',
                    buttonUrl: song.file.path
                });

                callback(null, true);
            }
        ],
        function (err, results) {
            // results is now equal to ['one', 'two']
            //console.log(err, results);
            console.log('sync done!');
            callbackMain(err, results);
        });
});

Meteor.methods({
    'orderAlbum': function(stripeToken, albumId) {
        return orderAlbumWrap(stripeToken, albumId, this.userId);
    },

    'orderSong': function (stripeToken, albumId, songId) {
        return orderSongWrap(stripeToken, albumId, songId, this.userId);
    }
});


////wrapping
//var wrappedDelayedMessage = Async.wrap(function (callback) {
//    setTimeout(function() {
//        callback(null, 'test test');
//    }, 3000);
//});
//
////usage
//Meteor.methods({
//    'delayedEcho': function() {
//        return wrappedDelayedMessage();
//    }
//});
