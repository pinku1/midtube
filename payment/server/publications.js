Meteor.publish('albumOrders', function(albumId) {
    return Order.find({userId: this.userId, albumId: albumId, status: 'succeeded'});
});

Meteor.publish('allPaymentPlans', function() {
    return PaymentPlan.find({isActive: true});
});
