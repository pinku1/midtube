OrderSong = React.createClass({
    mixins: [ReactMeteorData],

    propTypes: {
        song: React.PropTypes.object.isRequired,
        album: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            showModal: false
        };
    },

    getMeteorData() {
        var pub = Meteor.subscribe('albumOrders', this.props.album._id);

        return {
            orders: Order.find({
                userId: Meteor.userId(),
                albumId: this.props.album._id,
                songId: this.props.song._id}).fetch()
        };
    },

    showModalBox(ev) {
        //show modal box
        this.setState({
            showModal: true
        });
    },

    hideModalBox() {
        this.setState({
            showModal: false
        });
    },

    getSalePrice() {
        if (this.props.album.sales.isFree === false) {
            return this.props.album.sales.songPrice;
        }

        return false
    },

    submitPayment(stripeToken) {
        console.log(stripeToken);
        var root = this;
        //create order
        Meteor.call('orderSong', stripeToken, this.props.album._id, this.props.song._id, function (err, res) {
            if (err) {
                console.log(err);
                FlashMessages.sendError('Unable to process order!');
                return;
            }

            console.log('order details: ', res);
            //show confirmation
            FlashMessages.sendSuccess('Thank you, your order is successfully completed!');
            FlashMessages.sendInfo('You can now download music, we will also send a copy to your email.');

            //close modal
            root.hideModalBox();
        });
    },

    downloadSong() {
        return this.props.song.file.path;
    },

    render() {
        return <div style={{'display': 'inline'}}>
            { this.data.orders.length > 0 ?
                <a className="ui mini icon positive button" href={this.downloadSong()}>
                    Download
                </a>
                : ''}

            { this.data.orders.length <= 0 && this.getSalePrice() ?
                <div style={{'display': 'inline'}}>
                    <div className="ui mini icon positive button" onClick={this.showModalBox}>
                        Buy - ${this.getSalePrice()}
                    </div>
                    <CardModal
                        key={this.props.song._id}
                        onFormSubmit={this.submitPayment}
                        id={'model-' + this.props.song._id}
                        showModal={this.state.showModal}
                        hideModal={this.hideModalBox}
                        itemTitle={this.props.song.title}
                        itemPrice={this.props.album.sales.songPrice} />
                </div>
            : '' }
        </div>
    }
});
