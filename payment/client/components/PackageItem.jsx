PackageItem = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        packageItem: React.PropTypes.object.isRequired
    },

    render() {
        return <div className="item">
            <div className="image">
                <img src="https://placeholdit.imgix.net/~text?txtsize=33&txt=180%C3%97180&w=175&h=145" />
            </div>
            <div className="content">
                <a className="header">{this.props.packageItem.title}</a>
                <div className="meta">
                    <span className="cinema">
                        Cost: ${this.props.packageItem.planPrice}
                    </span>
                </div>
                <div className="description">
                    <p>{this.props.packageItem.description}</p>
                </div>
                <div className="extra">
                    <div className="ui right floated primary button">
                        Add
                        <i className="right add icon"></i>
                    </div>
                </div>
            </div>
        </div>
    }
});
