OrderAlbum = React.createClass({
    mixins: [ReactMeteorData],

    propTypes: {
        album: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            showModal: false
        };
    },

    getMeteorData() {
        var pub = Meteor.subscribe('albumOrders', this.props.album._id);

        return {
            orders: Order.find({userId: Meteor.userId(), albumId: this.props.album._id, songId: null}).fetch()
        };
    },

    getSalePrice() {
        if (!_.isUndefined(this.props.album.sales) && (_.isUndefined(this.props.album.sales.isFree) || this.props.album.sales.isFree === false)) {
            return this.props.album.sales.price;
        }

        return false
    },

    isAlbumFree() {
        return !_.isUndefined(this.props.album.sales.isFree) && this.props.album.sales.isFree === true;
    },

    showModalBox(ev) {
        //show modal box
        this.setState({
            showModal: true
        });
    },

    hideModalBox() {
        this.setState({
            showModal: false
        });
    },

    submitPayment(stripeToken) {
        console.log(stripeToken);
        var root = this;
        //create order
        Meteor.call('orderAlbum', stripeToken, this.props.album._id, function (err, res) {
            if (err) {
                console.log(err);
                FlashMessages.sendError('Unable to process order!');
                return;
            }

            console.log('order details: ', res);
            //show confirmation
            FlashMessages.sendSuccess('Thank you, order success!');

            //close modal
            root.hideModalBox();
        });
    },

    downloadAlbum() {
        return this.props.album.file.zip
    },

    render() {
        return <div>
            { this.data.orders.length > 0 || this.isAlbumFree() ?
                <a className="ui labeled icon green button" href={this.downloadAlbum()}>
                    <i className="download icon"></i>
                    Download
                </a>
                : ''}
            { this.data.orders.length <= 0 && this.getSalePrice() && !this.isAlbumFree() ?
                <div>
                    <button className="ui labeled icon green button" onClick={this.showModalBox}>
                        <i className="shop icon"></i>
                        Buy now - ${this.getSalePrice()}
                    </button>
                    <CardModal
                        key={this.props.album._id}
                        onFormSubmit={this.submitPayment}
                        id={'model-' + this.props.album._id}
                        showModal={this.state.showModal}
                        hideModal={this.hideModalBox}
                        itemTitle={this.props.album.title}
                        itemPrice={this.props.album.sales.price} />
                </div>
            : '' }
        </div>
    }
});
