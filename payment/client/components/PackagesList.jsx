PackagesList = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    componentDidMount: function() {
        SEO.set({
            title: 'All Packages',
            description: 'Description for this page',
            meta: {
                //'property="og:image"': 'http://locationofimage.com/image.png'
            }
        });
    },

    getMeteorData() {
        return {
            packages: PaymentPlan.find({isActive: true}).fetch()
        }
    },

    renderPackage() {
        return this.data.packages.map((packageItem) => {
            return <PackageItem
                key={packageItem._id}
                packageItem={packageItem} />
        });
    },

    render() {
        return <div className="main">
            <PageHeader title="Available Packages" />
            <div className="ui grid container page main">
                <div className="ui divided items" style={{'width':'100%'}}>
                    {this.renderPackage()}
                </div>
                <button className="large ui right floated primary button">
                    Submit
                </button>
            </div>
        </div>
    }
});
