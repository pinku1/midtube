CardModal = React.createClass({
    propTypes: {
        id: React.PropTypes.string.isRequired,
        onFormSubmit: React.PropTypes.func.isRequired,
        showModal: React.PropTypes.bool.isRequired,
        hideModal: React.PropTypes.func.isRequired,
        itemTitle: React.PropTypes.string.isRequired,
        itemPrice: React.PropTypes.number.isRequired
    },

    getInitialState() {
        return {
            number: '',
            expireMonth: '',
            expireYear: '',
            cvc: '',
            isLoading: false
        };
    },

    componentDidUpdate() {
        if (this.props.showModal && _.isNull(Meteor.userId())) {
            $('#loginModal')
                .modal({detachable: false})
                .modal('show');
            return;
        }

        $('#model-' + this.props.id)
            .modal({detachable: false})
            .modal(this.props.showModal ? 'show' : 'hide');
    },

    closeModal() {
        this.props.hideModal();
    },

    componentDidMount() {
        $(document).on('submit', 'form', function (e) {
            e.preventDefault();
        });

        $('.ui.dropdown.card-type').dropdown();

        $('.ui.form.payment')
            .form({
                on: 'blur',
                inline : true,
                fields: {
                    number: {
                        identifier  : 'number',
                        rules: [
                            {
                                type   : 'creditCard',
                                prompt : 'Please enter a valid credit card'
                            }
                        ]
                    },
                    cvc: {
                        identifier: 'cvc',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "CVC"'
                            }
                        ]
                    },
                    expireMonth: {
                        identifier: 'expireMonth',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Expiry Month"'
                            }
                        ]
                    },
                    expireYear: {
                        identifier: 'expireYear',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Expiry Year"'
                            }
                        ]
                    }
                }
            });
    },

    handleChange(e) {
        this.setState({
            number: ReactDOM.findDOMNode(this.refs.number).value,
            expireMonth: ReactDOM.findDOMNode(this.refs.expireMonth).value,
            expireYear: ReactDOM.findDOMNode(this.refs.expireYear).value,
            cvc: ReactDOM.findDOMNode(this.refs.cvc).value
        });
    },

    handleSubmit(ev) {
        //this.props.onFormSubmit(this.state);
        this.setState({isLoading: true});
        var root = this;
        Stripe.card.createToken({
            number: this.state.number,
            cvc: this.state.cvc,
            exp_month: this.state.expireMonth,
            exp_year: this.state.expireYear
        }, function(status, response) {
            console.log(status, response);
            if (!response.error) {
                //success
                var stripeToken = response.id;
                root.props.onFormSubmit(stripeToken);
            } else {
                console.log(response.error.message);
                FlashMessages.sendError(response.error.message);
                root.setState({isLoading: false});
            }
        });
    },

    render() {
        if (!_.isNull(Meteor.userId())) {
            return (
                <div className="ui modal" id={'model-' + this.props.id}>
                    <div className="header">
                        Payment Checkout
                    </div>
                    <div className="content">
                        <Loading active={this.state.isLoading} />

                        <h3 className="ui header" style={{'marginTop': '0', 'textAlign': 'center'}}>
                            <div className="content">
                                {this.props.itemTitle}
                                <div className="sub header">Amount to pay: <strong>${this.props.itemPrice}</strong></div>
                            </div>
                        </h3>

                        <div className="ui piled segment" style={{'margin': '10px 0px'}}>
                            <p>
                                Purchasing album includes unlimited streaming, plus high-quality digital download in MP3. Also we will send music files to your email + you can re-download album any time in future.
                            </p>
                        </div>

                        <form className="ui form payment">
                            <h4 className="ui dividing header">Card Information</h4>
                            <div className="field">
                                <label>Card Number</label>
                                <input
                                    type="text"
                                    name="number"
                                    ref="number"
                                    onChange={this.handleChange}
                                    value={this.state.number}
                                    maxLength="16"
                                    placeholder="Card #" />
                            </div>
                            <div className="fields">
                                <div className="ten wide field">
                                    <label>CVC</label>
                                    <input
                                        type="text"
                                        name="cvc"
                                        ref="cvc"
                                        onChange={this.handleChange}
                                        value={this.state.cvc}
                                        maxLength="3"
                                        placeholder="CVC" />
                                </div>
                                <div className="six wide field">
                                    <label>Expiration</label>
                                    <div className="two fields">
                                        <div className="field">
                                            <select
                                                className="ui fluid search dropdown"
                                                name="expireMonth"
                                                ref="expireMonth"
                                                onChange={this.handleChange}
                                                value={this.state.expireMonth}>
                                                <option value="">Month</option>
                                                <option value="1">January</option>
                                                <option value="2">February</option>
                                                <option value="3">March</option>
                                                <option value="4">April</option>
                                                <option value="5">May</option>
                                                <option value="6">June</option>
                                                <option value="7">July</option>
                                                <option value="8">August</option>
                                                <option value="9">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                        <div className="field">
                                            <input
                                                type="text"
                                                name="expireYear"
                                                ref="expireYear"
                                                onChange={this.handleChange}
                                                value={this.state.expireYear}
                                                maxLength="4"
                                                placeholder="Year" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="actions">
                        <div className="ui black button" onClick={this.closeModal}>
                            Close
                        </div>
                        <div className="ui labeled icon primary button" onClick={this.handleSubmit}>
                            <i className="right arrow icon"></i>
                            Pay
                        </div>
                    </div>
                </div>
            )
        } else {
            return <div></div>
        }
    }
});
