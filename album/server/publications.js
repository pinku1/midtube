Meteor.publish('featuredAlbums', function(category) {
    //show featured albums only
    var where = {featured: true, isActive: true};

    if (!_.isUndefined(category) && !_.isNull(category) && category !== 'all' && category !== '.com') {
        where['category.slug'] = category;
    }

    return Album.find(where, {sort: {createdAt: -1}, limit: 12});
});

Meteor.publish('topAlbums', function(category) {
    var where = {isActive: true};

    if (!_.isUndefined(category) && !_.isNull(category) && category !== 'all' && category !== '.com') {
        where['category.slug'] = category;
    }

    return Album.find(where, {sort: {baseScore: -1}, limit: 10});
});

Meteor.publish('relatedAlbums', function() {
    //todo show related albums
    return Album.find({featured: true, isActive: true}, {sort: {createdAt: -1}, limit: 12});
});

Meteor.publish('singleAlbum', function (id, isOwner) {
    var selector = {_id: id};
    
    //if asked to return owner's album only
    if (!_.isUndefined(isOwner)) {
        selector =_.extend(selector, {
            ownerId: this.userId
        });
    } else {
        //if not owner requesting
        selector = _.extend(selector, {isActive: true})
    }
    
    return Album.find(selector, {limit: 1});
});

Meteor.publish('findAlbums', function (query, limit) {
    //find by query
    var find = {isActive: true};
    if (!_.isNull(query)) {
        var queryRegex = ".*" + query + ".*";
        find = _.extend(find, {
            $or: [
                {title: {$regex: queryRegex, $options: 'i'}},
                {description: {$regex: queryRegex, $options: 'i'}},
                {artists: {$elemMatch: {fullName: {$regex: queryRegex, $options: 'i'}}}},
                {"recordLabel.title": {$regex: queryRegex, $options: 'i'}},
                {writerName: {$regex: queryRegex, $options: 'i'}},
                {tags: {$regex: queryRegex, $options: 'i'}}
            ]
        });
    }

    if (_.isUndefined(limit)) {
        limit = AppSettings.limit;
    }

    if (limit > 300) {
        limit = 300;
    }

    Counts.publish(this, 'total-albums-count', Album.find(find));

    return Album.find(find, {sort: {createdAt: -1, baseScore: -1}, limit: parseInt(limit)});
});

Meteor.publish('albumSongs', function (albumId) {
    return Song.find({albumId: albumId, isActive: true}, {sort: {createdAt: 1, trackNumber: 1}});
});
