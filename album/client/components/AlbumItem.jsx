AlbumItem = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    getMeteorData() {
        return {
            currentUserId: Meteor.userId()
        };
    },

    componentDidMount() {
        $('.album-dropdown').dropdown();
    },

    getAlbumUrl() {
        return FlowRouter.path('/album/:_id', {_id: this.props.album._id});
    },

    isOwner() {
        if (!_.isNull(this.data.currentUserId)) {
            return this.data.currentUserId === this.props.album.ownerId;
        }

        return false;
    },

    deleteItem(ev) {
        Meteor.call('removeAlbum', this.props.album._id);
    },

    render() {
        return  <div className="column">
            <div className="ui card artistItem">
                <a className="image" href={this.getAlbumUrl()}>
                    <AlbumImage album={this.props.album} />
                </a>
                <div className="content">
                    <a className="header" href={this.getAlbumUrl()}>
                        {truncate(titleize(this.props.album.title))}
                    </a>
                    <div className="meta">
                        by<ArtistsNames artists={this.props.album.artists} truncate={true} />
                    </div>
                </div>
                <div className="extra content">
                    <span className="right floated">
                        <i className="heart outline like icon"></i>
                        {this.props.album.favorites || 0}
                    </span>
                    <i className="thumbs up icon"></i>
                    {this.props.album.upVotes || 0}
                </div>
                { this.isOwner() ?
                    <div className="ui icon top left pointing dropdown float button album-dropdown">
                        <i className="ellipsis vertical icon"></i>
                        <div className="menu">
                            <a className="item" onClick={this.deleteItem}>Delete</a>
                        </div>
                    </div>
                    : ''
                }
            </div>
        </div>
    }
});
