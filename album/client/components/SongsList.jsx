SongsList = React.createClass({
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    getMeteorData() {
        var handle = Meteor.subscribe('albumSongs', this.props.album._id);
        var allSongs = Song.find({albumId: this.props.album._id}, {sort: {trackNumber: 1, createdAt: 1}}).fetch();

        if (handle.ready()) {
            if (!_.isUndefined(allSongs[0]) && !_.isNull(allSongs[0].file.path) && !_.isUndefined(Session.get('playerSong')) && _.isNull(Session.get('playerSong').file.path)) {
                console.log('load first song in player now');
                Session.set('playerSong', allSongs[0]);
            }
        }

        return {
            loading: ! handle.ready(),
            songs: allSongs
        };
    },

    renderSongs() {
        return this.data.songs.map((song) => {
            return <SongItem
                    key={song._id}
                    song={song}
                    album={this.props.album} />;
        });
    },

    renderUpdateSongs() {
        return this.data.songs.map((song) => {
            return <SongItemUpdate
                    key={song._id}
                    song={song}
                    album={this.props.album} />;
        });
    },

    render() {
        return <div style={{'width': '100%'}}>
            <Loading active={this.data.loading} />
            <table className="ui striped table">
                <thead>
                    <tr>
                        <th width="5%"></th>
                        <th width="5%">#</th>
                        <th width="25%">Title</th>
                        <th width="30%">Artist</th>
                        <th width="15%">Price</th>
                        <th width="20%"></th>
                    </tr>
                </thead>
                <tbody>
                    { (_.isUndefined(this.props.isUpdate) || this.props.isUpdate === false) ?
                        this.renderSongs()
                    :
                        this.renderUpdateSongs()
                    }
                </tbody>
            </table>
            { this.data.songs.length <= 0 ?
                <div className="ui message">
                    <div className="header">
                        No songs available for this album
                    </div>
                </div>
                :
                ''
            }
        </div>
    }
});
