SongImage = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    getPicture() {
        if (!_.isUndefined(this.props.album.cover)) {
            return this.props.album.cover.path;
        } else {
            return 'https://placeholdit.imgix.net/~text?txtsize=33&txt=180%C3%97180&w=180&h=180';
        }
    },

    render() {
        return <img className="ui avatar image" src={this.getPicture()} title={this.props.album.title} />
    }
});
