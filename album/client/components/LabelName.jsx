LabelName = React.createClass({
    propTypes: {
        recordLabel: React.PropTypes.object.isRequired
    },

    getPermalink() {
        if (!_.isNull(this.props.recordLabel.slug)) {
            return FlowRouter.path('/profile/:slug', {slug: this.props.recordLabel.slug});
        } else {
            return FlowRouter.path('/search/:query', {query: this.props.recordLabel.title});
        }
    },

    render() {
        return <span>
            <a className="artistName" href={this.getPermalink()}>
                {titleize(this.props.recordLabel.title)}
            </a>
        </span>
    }
});
