AlbumsList =  React.createClass({
    componentDidMount: function() {
        SEO.set({
            title: 'All Albums',
            description: 'Description for this template',
            meta: {
                //'property="og:image"': 'http://locationofimage.com/image.png'
            }
        });

        $('.menu .item').tab();
    },

    render() {
        return <div className="main">
            <PageHeader title="Recent albums" />
            <div className="main ui grid container">
                <AlbumResult />
            </div>
        </div>
    }
});
