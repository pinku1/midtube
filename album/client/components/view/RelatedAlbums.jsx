RelatedAlbums = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    getMeteorData() {
        var handle = Meteor.subscribe('relatedAlbums');

        return {
            loading: ! handle.ready(),
            albums: Album.find({featured: true, _id: {$ne: this.props._id}}, {sort: {createdAt: -1}, limit: 12}).fetch()
        }
    },

    renderAlbums() {
        return this.data.albums.map((album) => {
            return <AlbumItem
                key={album._id}
                album={album}/>
        });
    },

    render() {
        return <div className="ui grid container main">
            <Loading active={this.data.loading} />
            { this.data.albums.length > 0 ?
                <div className="ui doubling centered six column grid" style={{'width': '100%'}}>
                    {this.renderAlbums()}
                </div>
                :
                <div className="ui message">
                    <div className="header">
                        No related albums found.
                    </div>
                </div>
            }
        </div>
    }
});
