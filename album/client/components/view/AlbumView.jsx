AlbumView = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        _id: React.PropTypes.string.isRequired
    },

    componentWillMount: function() {
        if (!_.isUndefined(this.data.album)) {
            var artistName = '';
            if (!_.isUndefined(this.data.album.artists[0])) {
                artistName = this.data.album.artists[0].fullName;
            }
            SEO.set({
                title: this.data.album.title + ' by ' + artistName + ' - Listen and download on MidTube.com' ,
                description: 'Listen and download ' + this.data.album.title + ' by ' + artistName + ' plus share and discover new music!',
                meta: {
                    'property="og:image"': !_.isNull(this.data.album.cover.path) ? this.data.album.cover.path.replace(/ /g, '%20') : 'http://www.midtube.com/images/album2.jpg'
                }
            });
        }
    },

    getMeteorData() {
        var album = Album.findOne(this.props._id);
        if (_.isUndefined(album)) {
            //todo album not found
            //FlowRouter.go("/not-found");
        }

        return {
            album: album,
            isMobile: Session.get('isMobile')
        }
    },

    render() {
        return (
            <div>
                {!_.isUndefined(this.data.album) ?
                    <div>
                        <Parallax bgImage="/images/blur.jpg" strength={300}>
                            <AlbumViewHead album={this.data.album} />
                        </Parallax>

                        <AlbumActionArea album={this.data.album} />

                        <div className={classNames('ui centered grid', { 'one column': this.data.isMobile, 'three column': !this.data.isMobile })}
                             style={{'padding': '0 10px'}}>
                            <div className="column">
                                <MusicPlayer />
                            </div>
                        </div>

                        <div style={{'backgroundColor': '#fff', 'padding': '40px 0', 'margin': '40px 0'}}>
                            <div className="ui grid container">
                                <h2 className="ui header" style={{'width': '100%'}}>Tracks</h2>
                                <SongsList album={this.data.album} />
                            </div>
                        </div>

                        <div className="ui grid container">
                            <Disqus />
                        </div>

                        <div className="ui section divider"></div>

                        <PageHeader title="Related Music" />
                        <RelatedAlbums />
                    </div>
                : 'loading...' }
                <DoReload currentPage={this.props._id} />
            </div>
        )
    }
});
