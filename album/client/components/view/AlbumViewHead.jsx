AlbumViewHead = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    componentDidMount() {
        //color thief back overlay
        //if (!_.isNull(this.props.album.cover.path)) {
        //    var image = new Image();
        //    image.crossOrigin = "anonymous";
        //    image.src = this.props.album.cover.path;
        //    setTimeout(function () {
        //        var colorThief = new ColorThief.colorRob();
        //        var res = colorThief.getColor(image);
        //
        //        if (!_.isUndefined(res[0])) {
        //            $(".react-parallax-content").css("background-color", "rgba(" + res[0] + "," + res[1] + "," + res[2] + ",0.2)");
        //        }
        //    }, 1200);
        //}
    },

    render() {
        return <div className="ui grid container">
            <h2 className="ui header artist">
                <AlbumImage album={this.props.album} />
                <div className="content" style={{'color': '#fff'}}>
                    {titleize(this.props.album.title)}
                    <div className="sub header">
                        by&nbsp;
                        <ArtistsNames artists={this.props.album.artists} /><br />
                        {(!_.isNull(this.props.album.recordLabel.title) && this.props.album.recordLabel.title.length > 0) ?
                            <small>
                                Label: <LabelName recordLabel={this.props.album.recordLabel} />
                            </small>
                        : '' }
                    </div>
                    <div className="sub header details">
                        Release Date: {humanDate(this.props.album.releaseDate)}
                    </div>
                    <div className="sub header details">
                        <p dangerouslySetInnerHTML={{__html: this.props.album.description.replace(/\r?\n/g, '<br />')}} />
                    </div>
                </div>
            </h2>
        </div>
    }
});
