AlbumActionArea = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    getMeteorData() {
        return {
            currentUserId: Meteor.userId(),
            currentUser: Meteor.user()
        };
    },

    hasFavorited() {
        if (_.isUndefined(this.props.album.favoriteBy) || this.props.album.favoriteBy.length <= 0) {
            return false;
        }

        return _.indexOf(this.props.album.favoriteBy, this.data.currentUserId) != '-1';
    },

    hasUpVoted() {
        if (_.isUndefined(this.props.album.upVoters) || this.props.album.upVoters.length <= 0) {
            return false;
        }

        return _.indexOf(this.props.album.upVoters, this.data.currentUserId) != '-1';
    },

    doFavorite() {
        this.handleVoteClick('favoriteAlbum');
    },

    doUpVote() {
        this.handleVoteClick('upvoteAlbum');
    },

    handleVoteClick(meteorMethodName) {
        //e.preventDefault();
        if (_.isNull(this.data.currentUser)) {
            console.log('login first!');
            $('#loginModal')
                .modal({detachable: false})
                .modal('show');
            //FlowRouter.go('/login');
            //flashMessage('Please login first', 'info');
        } else {
            Meteor.call(meteorMethodName, this.props.album, function (error, result) {
                //console.log(error);
                //console.log(result);
                FlashMessages.send('Success!');
            });
        }
    },

    getAlbumUpdateUrl() {
        return FlowRouter.path('/album/update/info/:_id', {_id: this.props.album._id});
    },

    isOwner: function () {
        return (!_.isNull(this.data.currentUserId) && (this.props.album.ownerId == this.data.currentUserId));
    },

    getLinks: function (name) {
        if (!_.isUndefined(this.props.album.links)) {
            if (!_.isNull(this.props.album.links[name]) && this.props.album.links[name].length > 2) {
                return this.props.album.links[name];
            }
        }

        return false;
    },

    getShareTitle() {
        var title = s.capitalize(this.props.album.title);
        if (!_.isUndefined(this.props.album.artists) && !_.isUndefined(this.props.album.artists[0])) {
            title += ' by ';
            title += s.capitalize(this.props.album.artists[0].fullName);
        }

        console.log(title);
        return title;
    },

    render() {
        return (
            <div className="ui grid container" style={{'margin': '10px 0'}}>
                <div className="ui compact stackable menu action" style={{'width': '100%'}}>
                    <div className="item">
                        <button className="ui labeled button" tabIndex="0" onClick={this.doUpVote}>
                            <div className="ui red button">
                                <i className="thumbs up icon"></i> {this.hasUpVoted() ? 'Liked' : 'Like'}
                            </div>
                            <a className="ui basic red left pointing label" style={{'paddingBottom': '9px'}}>
                                {this.props.album.upVotes || 0}
                            </a>
                        </button>
                    </div>
                    <div className="item">
                        <OrderAlbum album={this.props.album} />
                    </div>
                    <a className="item" onClick={this.doFavorite}>
                        <i className={this.hasFavorited() ? 'icon red heart' : 'icon red empty heart'}></i> {this.hasFavorited() ? 'Favorited' : 'Favorite'}
                    </a>
                    <div className="item">
                        <SocialSharePopUp title={this.getShareTitle()} />
                    </div>
                    {this.isOwner() ?
                        <a className="item" href={this.getAlbumUpdateUrl()}>
                            <i className="setting icon"></i>
                            Edit Album
                        </a>
                        : '' }
                    <div className="right menu">
                        {this.getLinks('itunes') ? <a className="item" href={this.getLinks('itunes')} target="_blank"><i className="apple icon"></i></a> : '' }
                        {this.getLinks('googlePlay') ? <a className="item" href={this.getLinks('googlePlay')} target="_blank"><i className="google icon"></i></a> : '' }
                        {this.getLinks('soundCloud') ? <a className="item" href={this.getLinks('soundCloud')} target="_blank"><i className="soundcloud icon"></i></a> : '' }
                        {this.getLinks('amazon') ? <a className="item" href={this.getLinks('amazon')} target="_blank"><i className="adn icon"></i></a> : '' }
                    </div>
                </div>
            </div>
        )
    }
});
