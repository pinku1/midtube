SongItem = React.createClass({
    mixins: [ReactMeteorData],

    propTypes: {
        song: React.PropTypes.object.isRequired,
        album: React.PropTypes.object.isRequired
    },

    getMeteorData() {
        return {
            playerStatus: Session.get('playerStatus'),
            currentSong: Session.get('playerSong')
        };
    },

    handleSongChange(ev) {
        var isDifferentSong = false;
        var currentStatus = Session.get('playerStatus');
        var root = this;

        async.series([
            function(callback){
                cl('check song is diff');
                if (_.isUndefined(Session.get('playerSong'))) {
                    isDifferentSong = true;
                } else {
                    isDifferentSong = root.props.song._id !== Session.get('playerSong')._id;
                }
                cl('is different' + isDifferentSong);
                callback(null, true);
            },
            function(callback){
                if (isDifferentSong) {
                    cl('if different song pause current first');
                    //if different songs requested pause first then play another
                    Session.set('playerStatus', Sound.status.STOPPED);
                    currentStatus = Sound.status.STOPPED;

                    setTimeout(function(){
                        callback(null, true);
                    }, 50);
                } else {
                    callback(null, true);
                }
            },
            function(callback){
                cl('set requested song');
                if (isDifferentSong) {
                    setTimeout(function(){
                        Session.set('playerSong', root.props.song);

                        callback(null, true);
                    }, 50);
                } else {
                    callback(null, true);
                }
            },
            function(callback){
                if (currentStatus !== Sound.status.PLAYING) {
                    //update song play counter
                    //todo log unique counts only
                    Meteor.call('updateSongCounter', root.props.song._id);

                    setTimeout(function(){
                        cl('play new song done');
                        Session.set('playerStatus', Sound.status.PLAYING);

                        callback(null, true);
                    }, 200);
                } else {
                    cl('ok pause song done');
                    Session.set('playerStatus', Sound.status.PAUSED);

                    callback(null, true);
                }
            }
        ]);
        //
        //if (isDifferentSong) {
        //    //if different songs requested pause first then play another
        //    Session.set('playerStatus', Sound.status.PAUSED);
        //}
        //
        //Session.set('playerSong', this.props.song);
        //
        //if (Session.get('playerStatus') !== Sound.status.PLAYING) {
        //    //update song play counter
        //    //todo log unique counts only
        //    Meteor.call('updateSongCounter', this.props.song._id);
        //
        //    setTimeout(function(){
        //        Session.set('playerStatus', Sound.status.PLAYING);
        //    }, 1000);
        //} else {
        //    Session.set('playerStatus', Sound.status.PAUSED);
        //}
    },

    isPlaying() {
        if (!_.isUndefined(this.data.currentSong) && !_.isUndefined(this.data.currentSong.file)) {
            return this.props.song.file.path === this.data.currentSong.file.path && Session.get('playerStatus') === Sound.status.PLAYING;
        } else {
            return false;
        }
    },

    render() {
        return <tr className={classNames({ 'positive': this.isPlaying() })}>
            <td onClick={this.handleSongChange}>
                {this.isPlaying() ? <i className="volume up icon"></i> : <i className="play icon"></i>}
            </td>
            <td>{s.pad(this.props.song.trackNumber, 2, '0')}</td>
            <td onClick={this.handleSongChange}>
                {titleize(this.props.song.title)}
            </td>
            <td><ArtistsNames artists={this.props.song.artists} /></td>
            <td>${this.props.album.sales.songPrice || 0}</td>
            <td style={{'textAlign': 'right'}}>
                <div className="ui label">
                    plays: {this.props.song.playCount || 0}
                </div>
                <OrderSong
                    song={this.props.song}
                    album={this.props.album}
                    />
            </td>
        </tr>
    }
});
