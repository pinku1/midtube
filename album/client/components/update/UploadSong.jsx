UploadSong = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            progress: 0
        };
    },

    uploadFile(event) {
        var root = this;
        var metaContext = {songId: this.props.album._id, type: 'song'};
        var uploader = new Slingshot.Upload("songUploads", metaContext);
        var albumId = this.props.album._id;
        var file = event.target.files[0];

        console.log(file);

        //progress bar
        var computation = Tracker.autorun(function () {
            var progress = Math.ceil(uploader.progress() * 100);
            if (progress === parseInt(progress, 10)) {
                root.setState({progress: progress});
            }
        });

        uploader.send(file, function (error, downloadUrl) {
            computation.stop(); // Stop the computation in order to save memory.

            if (error) {
                // Log service detailed response.
                console.error('Error uploading', uploader.xhr.response);
                console.log(error);
            }
            else {
                var songData = {
                    title: file.name.slice(0, -4),
                    file: {
                        name: file.name,
                        path: downloadUrl,
                        size: file.size,
                        type: file.type
                    },
                    albumId: albumId
                };

                //console.log(songData);
                Meteor.call('addSong', songData, function () {
                    console.log('song added!');
                    FlashMessages.sendSuccess('Song uploaded successfully!');
                });
            }
        });
    },

    renderProgressBar() {
        var progressStyle = {transitionDuration: '300ms', width: this.state.progress + '%'};
        var progressClass = classNames({
            'ui': true,
            'active': this.state.progress > 0,
            'progress': true,
            'success': this.state.progress === 100,
            'hidden': this.state.progress === 0
        });

        return <div className={progressClass} data-percent={this.state.progress}>
            <div className="bar" style={progressStyle}>
                <div className="progress">{this.state.progress}</div>
            </div>
            <div className="label">Uploading Files</div>
        </div>
    },

    triggerInput() {
        $('#albumSongInput').click();
    },

    render() {
        return <div>
            <button className="ui primary button" onClick={this.triggerInput}  style={{'margin': '10px 0'}}>
                <i className="upload icon"></i> Upload Song
            </button>
            <p><em>only mp3 files are allowed for now upto size of 100mb</em></p>
            <input
                ref="file"
                type="file"
                name="file"
                id="albumSongInput"
                onChange={this.uploadFile}
                style={{'display': 'none'}} />
            {this.renderProgressBar()}
        </div>
    }
});
