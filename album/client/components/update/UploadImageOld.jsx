UploadImageOld = React.createClass({
    mixins: [ReactMeteorData],

    getMeteorData() {
        var sub = Meteor.subscribe('images');
        return {
            images: Images.find().fetch()
        };
    },

    uploadFile(event) {
        var files = event.target.files;
        //for (var i = 0, ln = files.length; i < ln; i++) {
        FS.Utility.eachFile(event, function(file) {
            Images.insert(file, function (err, fileObj) {
                // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
                //console.log(err, fileObj);
            });
        });
    },

    renderImages() {
        return this.data.images.map((image) => {
            //console.log(image);
            //console.log(image.isImage());
            //console.log(image.isAudio());
            return <div>
                <a href={image.url({store:'images'})} target="_blank">
                    <img src={image.url({store:'thumbs'})} />
                </a>
            </div>
        });
    },

    render() {
        return <div>
            <input ref="file" type="file" name="file" onChange={this.uploadFile} />
            {this.renderImages()}
        </div>
    }
});
