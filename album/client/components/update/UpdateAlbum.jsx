UpdateAlbum = React.createClass({
    mixins: [ReactMeteorData, SmoothScrollMixin],

    componentDidMount: function() {
        SEO.set({
            title: 'Update Album',
            description: 'Description for this template',
            meta: {
              //'property="og:image"': 'http://locationofimage.com/image.png'
            }
          });
    },

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        _id: React.PropTypes.string.isRequired
    },

    getMeteorData() {
        return {
            album: Album.findOne(this.props._id)
        };
    },

    componentWillMount() {
        if (_.isUndefined(this.data.album)) {
            //if no album returned, redirect back to home page
            FlowRouter.go('/');
        }
    },

    render() {
        return <div className="ui grid container main">
            <AlbumUpdateSteps
                albumId={this.props._id}
                active="info" />

            <h2 className="ui top attached segment header">
                <div className="content">
                    Update Album Information
                </div>
            </h2>
            <div className="ui attached segment">
                <UploadAlbumPicture album={this.data.album} />
                <AlbumInfoForm album={this.data.album} />
            </div>
        </div>
    }
});
