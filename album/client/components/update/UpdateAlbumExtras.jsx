UpdateAlbumExtrasForm = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    componentDidMount() {
        //$('select.dropdown.isFree').dropdown();
    },

    getInitialState() {
        //console.log(this.props.artist);
        if (_.isUndefined(this.props.album.sales)) {
            this.props.album['sales'] = {};
        }

        return {
            isFree: _.isUndefined(this.props.album.sales.isFree) ? true : this.props.album.sales.isFree,
            price: this.props.album.sales.price || '9.99',
            songPrice: this.props.album.sales.songPrice || '0.99'
        };
    },

    handleChange(e) {
        this.setState({
            isFree: (ReactDOM.findDOMNode(this.refs.isFree).value === 'true'),
            price: ReactDOM.findDOMNode(this.refs.price).value,
            songPrice: ReactDOM.findDOMNode(this.refs.songPrice).value
        });
    },

    handleBlur() {
        this.updateWithNewData({
            isFree: (ReactDOM.findDOMNode(this.refs.isFree).value === 'true'),
            price: ReactDOM.findDOMNode(this.refs.price).value.trim(),
            songPrice: ReactDOM.findDOMNode(this.refs.songPrice).value.trim()
        });
    },

    updateWithNewData(newData) {
        console.log('update album: ', newData);

        Meteor.call('updateAlbum', this.props.album._id, {
            sales: {
                isFree: newData.isFree,
                price: newData.price,
                songPrice: newData.songPrice
            }
        });
    },

    handleSubmit(event, template) {
        event.preventDefault();
        console.log('handle submit');
        this.handleBlur();

        FlowRouter.go('/album/:_id', {_id: this.props.album._id});
    },

    render() {
        var isFree = this.state.isFree || this.state.isFree === 'true';

        return <form className="ui form" onSubmit={this.handleSubmit}>
            <div className="field">
                <label>Is Album Paid? *</label>
                <select 
                    name="isFree"
                    ref="isFree"
                    className="ui fluid dropdown isFree"
                    value={this.state.isFree ? "true" : "false"}
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}>
                    <option value="false">Yes</option>
                    <option value="true">No</option>
                </select>
            </div>
            <div className="field" style={{display: isFree ? 'none' : 'block'}}>
                <label>Album Price ($)</label>
                <input
                    name="price"
                    ref="price"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.price}
                    placeholder="0.00"/>
            </div>
            <div className="field" style={{display: isFree ? 'none' : 'block'}}>
                <label>Per Song Price ($)</label>
                <input
                    name="songPrice"
                    ref="songPrice"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.songPrice}
                    placeholder="0.00"/>
            </div>

            <button type="submit" className="ui right labeled icon blue button submit">
                <i className="upload icon"></i>
                Submit
            </button>
        </form>
    }
});

UpdateAlbumExtras = React.createClass({
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        _id: React.PropTypes.string.isRequired
    },

    getMeteorData() {
        return {
            album: Album.findOne(this.props._id)
        };
    },

    componentDidMount: function() {
        SEO.set({
            title: 'Update Album',
            description: 'Description for this template',
            meta: {
                //'property="og:image"': 'http://locationofimage.com/image.png'
            }
        });
    },

    componentWillMount() {
        if (_.isUndefined(this.data.album)) {
            //if no album returned, redirect back to home page
            FlowRouter.go('/');
        }
    },

    render() {
        return <div className="ui grid main container">
            <AlbumUpdateSteps
                albumId={this.data.album._id}
                active="extras" />

            <h2 className="ui top attached segment header">
                <div className="content">
                    Update Album Extras
                </div>
            </h2>
            <div className="ui attached segment">
                <UpdateAlbumExtrasForm album={this.data.album} />
            </div>
        </div>
    }
});
