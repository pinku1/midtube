UploadAlbumPicture  = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            progress: 0,
            isLoading: false
        };
    },

    uploadFile(event) {
        var root = this;
        var metaContext = {albumId: this.props.album._id, type: 'album'};
        var uploader = new Slingshot.Upload("imageUploads", metaContext);
        var albumId = this.props.album._id;
        var file = event.target.files[0];
        //show loading
        this.setState({isLoading: true});

        //progress bar
        var computation = Tracker.autorun(function () {
            var progress = Math.ceil(uploader.progress() * 100);
            if (progress === parseInt(progress, 10)) {
                root.setState({progress: progress});
            }
        });

        console.log(file);
        async.series([
                function(callback){
                    //upload original
                    uploader.send(file, function (error, downloadUrl) {
                        if (error) {
                            // Log service detailed response.
                            console.error('Error uploading', uploader.xhr.response);
                            console.log(error);
                        }
                        else {
                            console.log(downloadUrl);

                            callback(error, {size: file.size, type: file.type, pathOriginal: downloadUrl});
                        }
                    });
                },
                function(callback){
                    //upload resized
                    Resizer.resize(file, {width: 300, height: 300, cropSquare: true}, function(err, fileCropped) {
                        uploader.send(fileCropped, function (error, downloadUrl) {
                            if (error) {
                                // Log service detailed response.
                                console.error('Error uploading', uploader.xhr.response);
                                console.log(error);
                            }
                            else {
                                console.log(downloadUrl);

                                callback(error, {path: downloadUrl});
                            }
                        });

                    });
                }
            ],
            function(err, results){
                computation.stop(); // Stop the computation in order to save memory.

                Meteor.call('updateAlbum', albumId, {cover: _.extend(results[0], results[1])}, function () {
                    console.log('all done');//hide loading
                    root.setState({isLoading: false});

                    FlashMessages.sendSuccess('Picture uploaded successfully!');
                });
            });
    },

    renderProgressBar() {
        var progressStyle = {transitionDuration: '300ms', width: this.state.progress + '%'};
        var progressClass = classNames({
            'ui': true,
            'active': this.state.progress > 0,
            'progress': true,
            'success': this.state.progress === 100,
            'hidden': this.state.progress === 0
        });

        return <div className={progressClass} data-percent={this.state.progress}>
            <div className="bar" style={progressStyle}>
                <div className="progress">{this.state.progress}</div>
            </div>
            <div className="label">Uploading Files</div>
        </div>
    },

    triggerInput() {
        $('#albumPictureInput').click();
    },

    render() {
        return <div>
            <Loading active={this.state.isLoading} />
            <AlbumImage album={this.props.album} float='left' />
            {this.renderProgressBar()}
            <button className="ui primary button" onClick={this.triggerInput}  style={{'margin': '10px 0'}}>
                <i className="upload icon"></i> Upload Picture
            </button>
            <input
                ref="file"
                type="file"
                name="file"
                id="albumPictureInput"
                onChange={this.uploadFile}
                style={{'display': 'none'}} />
        </div>
    }
});