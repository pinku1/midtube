UpdateAlbumSongs = React.createClass({
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        _id: React.PropTypes.string.isRequired
    },

    getMeteorData() {
        return {
            album: Album.findOne(this.props._id)
        };
    },

    componentDidMount: function() {
        SEO.set({
            title: 'Update Album',
            description: 'Description for this template',
            meta: {
                //'property="og:image"': 'http://locationofimage.com/image.png'
            }
        });
    },

    componentWillMount() {
        if (_.isUndefined(this.data.album)) {
            //if no album returned, redirect back to home page
            FlowRouter.go('/');
        }
    },

    handleSubmit(event) {
        event.preventDefault();

        FlowRouter.go('/album/update/links/:_id', {_id: this.data.album._id});
    },

    render() {
        return <div className="ui grid main container">
            <AlbumUpdateSteps
                albumId={this.data.album._id}
                active="songs" />

            <h2 className="ui top attached segment header">
                <div className="content">
                    Album Songs
                </div>
            </h2>
            <div className="ui attached segment">
                <UploadSong album={this.data.album} />

                <SongsList album={this.data.album} isUpdate={true} />

                <a className="ui right labeled icon blue button" onClick={this.handleSubmit}>
                    <i className="right arrow icon"></i>
                    Next
                </a>
            </div>
        </div>
    }
});