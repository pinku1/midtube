AlbumUpdateSteps = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        albumId: React.PropTypes.string.isRequired,
        active: React.PropTypes.string.isRequired
    },

    render() {
        return <div className="ui four steps small">
            <a className={classNames('step', {'active': this.props.active === 'info'})} href={FlowRouter.path('/album/update/info/:_id', {_id: this.props.albumId})}>
                <i className="info icon"></i>
                <div className="content">
                    <div className="title">Album Info</div>
                </div>
            </a>
            <a className={classNames('step', {'active': this.props.active === 'songs'})} href={FlowRouter.path('/album/update/songs/:_id', {_id: this.props.albumId})}>
                <i className="sound icon"></i>
                <div className="content">
                    <div className="title">Songs</div>
                </div>
            </a>
            <a className={classNames('step', {'active': this.props.active === 'links'})} href={FlowRouter.path('/album/update/links/:_id', {_id: this.props.albumId})}>
                <i className="share icon"></i>
                <div className="content">
                    <div className="title">Links</div>
                </div>
            </a>
            <a className={classNames('step', {'active': this.props.active === 'extras'})} href={FlowRouter.path('/album/update/extras/:_id', {_id: this.props.albumId})}>
                <i className="browser icon"></i>
                <div className="content">
                    <div className="title">Extras</div>
                </div>
            </a>
        </div>
    }
});