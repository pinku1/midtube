AlbumActiveToggle =  React.createClass({
    propTypes: {
        album: React.PropTypes.object.isRequired
    },

    getInitialState: function () {
        return {
            isActive: this.props.album.isActive
        };
    },

    handleChange() {
        var checked = ReactDOM.findDOMNode(this.refs.biography).checked;

        this.setState({
            isActive: checked
        });

        Meteor.call('updateAlbum', this.props.album._id, {
            isActive: checked
        }, function (err, res) {
            if (!err) {
                FlashMessages.sendSuccess(checked ? 'Album is now published.' : 'Album is not now un-published.');
            }
        });
    },

    render() {
        return (
            <div className="ui fitted slider checkbox">
                {this.props.album.isActive}
                <input
                    type="checkbox"
                    ref="isActive"
                    checked={this.state.isActive}
                    onChange={this.handleChange} /> <label></label>
            </div>
        )
    }
});
