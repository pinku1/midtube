UpdateAlbumLinksForm = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    getInitialState() {
        //console.log(this.props.artist);
        if (_.isUndefined(this.props.album.links)) {
            this.props.album['links'] = {};
        }

        return {
            itunes: this.props.album.links.itunes || '',
            googlePlay: this.props.album.links.googlePlay || '',
            soundCloud: this.props.album.links.soundCloud || '',
            amazon: this.props.album.links.amazon || ''
        };
    },

    handleChange(e) {
        this.setState({
            itunes: ReactDOM.findDOMNode(this.refs.itunes).value,
            googlePlay: ReactDOM.findDOMNode(this.refs.googlePlay).value,
            soundCloud: ReactDOM.findDOMNode(this.refs.soundCloud).value,
            amazon: ReactDOM.findDOMNode(this.refs.amazon).value
        });
    },

    handleBlur() {
        this.updateWithNewData({
            itunes: ReactDOM.findDOMNode(this.refs.itunes).value.trim(),
            googlePlay: ReactDOM.findDOMNode(this.refs.googlePlay).value.trim(),
            soundCloud: ReactDOM.findDOMNode(this.refs.soundCloud).value.trim(),
            amazon: ReactDOM.findDOMNode(this.refs.amazon).value.trim()
        });
    },

    updateWithNewData(newData) {
        console.log('update album: ', newData);

        Meteor.call('updateAlbum', this.props.album._id, {
            links: {
                itunes: newData.itunes,
                googlePlay: newData.googlePlay,
                soundCloud: newData.soundCloud,
                amazon: newData.amazon
            }
        });
    },

    handleSubmit(event, template) {
        event.preventDefault();
        console.log('handle submit');
        this.handleBlur();

        FlowRouter.go('/album/update/extras/:_id', {_id: this.props.album._id});
    },

    render() {
        return <form className="ui form" onSubmit={this.handleSubmit}>
            <div className="field">
                <label>iTunes Link</label>
                <input
                    name="itunes"
                    ref="itunes"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.itunes}
                    placeholder="Album iTunes link"/>
            </div>
            <div className="field">
                <label>Google Play Link</label>
                <input
                    name="googlePlay"
                    ref="googlePlay"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.googlePlay}
                    placeholder="Album google play link"/>
            </div>
            <div className="field">
                <label>SoundCloud Link</label>
                <input
                    name="soundCloud"
                    ref="soundCloud"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.soundCloud}
                    placeholder="Album SoundCloud link"/>
            </div>
            <div className="field">
                <label>Amazon Link</label>
                <input
                    name="amazon"
                    ref="amazon"
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.amazon}
                    placeholder="Album amazon link"/>
            </div>

            <button type="submit" className="ui right labeled icon blue button">
                <i className="right arrow icon"></i>
                Next
            </button>
        </form>
    }
});

UpdateAlbumLinks = React.createClass({
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        _id: React.PropTypes.string.isRequired
    },

    getMeteorData() {
        return {
            album: Album.findOne(this.props._id)
        };
    },

    componentDidMount: function() {
        SEO.set({
            title: 'Update Album',
            description: 'Description for this template',
            meta: {
                //'property="og:image"': 'http://locationofimage.com/image.png'
            }
        });
    },

    componentWillMount() {
        if (_.isUndefined(this.data.album)) {
            //if no album returned, redirect back to home page
            FlowRouter.go('/');
        }
    },

    render() {
        return <div className="ui grid main container">
            <AlbumUpdateSteps
                albumId={this.data.album._id}
                active="links" />

            <h2 className="ui top attached segment header">
                <div className="content">
                    Update Album External Links
                </div>
            </h2>
            <div className="ui attached segment">
                <UpdateAlbumLinksForm album={this.data.album} />
            </div>
        </div>
    }
});
