AddAlbumPicture = React.createClass({
    mixins: [ReactMeteorData],

    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        _id: React.PropTypes.string.isRequired
    },

    getMeteorData() {
        //var sub = Meteor.subscribe('singleAlbum', this.props._id);
        return {
            album: Album.findOne(this.props._id)
        };
    },

    handleSubmit(event) {
        event.preventDefault();

        FlowRouter.go('/album/update/songs/:_id', {_id: this.data.album._id});
    },

    render() {
        return <div className="ui grid container main">
            <h2 className="ui top attached segment header">
                <div className="content">
                    Add Album Picture
                </div>
            </h2>
            <div className="ui attached segment">
                <UploadAlbumPicture album={this.data.album} />

                <a className="ui right labeled icon blue button" onClick={this.handleSubmit}>
                    <i className="right arrow icon"></i>
                    Next
                </a>
            </div>
        </div>
    }
});
