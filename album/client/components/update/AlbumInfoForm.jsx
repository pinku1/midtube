AlbumInfoForm = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    propTypes: {
        album: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            title: this.props.album.title || '',
            writerName: this.props.album.writerName || '',
            recordLabel: this.props.album.recordLabel || {},
            description: this.props.album.description || '',
            releaseDate: this.props.album.releaseDate || '',
            tags: this.props.album.tags || [],
            categoryId: !_.isUndefined(this.props.album.category) ? this.props.album.category._id || ''  : '',
            category: this.props.album.category || {},
            artists: this.props.album.artists || []
        };
    },

    getMeteorData() {
        Meteor.subscribe('allCategories');

        return {
            categories: Category.find({isActive: true}, {sort: {title: 1}}).fetch()
        };
    },

    componentDidMount() {
        $(document).on('submit','form',function(e) {
            e.preventDefault();
        });

        $('.ui.form')
            .form({
                fields: {
                    title: {
                        identifier: 'title',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Album Name"'
                            }
                        ]
                    },
                    artistName: {
                        identifier: 'artistName',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Artist Name"'
                            }
                        ]
                    },
                    recordLabelTitle: {
                        identifier: 'recordLabelTitle',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Record Label"'
                            }
                        ]
                    },
                    releaseDate: {
                        identifier: 'releaseDate',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Release Date"'
                            }
                        ]
                    },
                    categoryId: {
                        identifier: 'categoryId',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please select "Category"'
                            }
                        ]
                    }
                }
            });

        //$('select.dropdown.genre').dropdown();
    },

    handleSubmit(event, template) {
        event.preventDefault();
        //console.log('handle submit');

        var dataToSend = this.state;
        if (_.isEmpty(this.props.album)) {
            //is add
            Meteor.call('createAlbum', dataToSend, function (error, result) {
                if (error) {
                    console.log(error);
                    return;
                }

                //on done go to add picture page
                setTimeout(function(){
                    //FlowRouter.go('/album/add/picture/:_id', {_id: result});
                    //todo fix this and remove window reload
                    window.location.href = window.location.origin + '/album/add/picture/' + result._id;
                }, 1000);
            });
        } else {
            //is update
            var albumId = this.props.album._id;
            Meteor.call('updateAlbum', albumId, dataToSend, function (error, result) {
                if (error) {
                    console.log(error);
                    return;
                }

                //on done go to update page
                FlowRouter.go('/album/update/songs/:_id', {_id: albumId});
            });
        }
    },

    handleChange(e) {
        this.setState({
            title: ReactDOM.findDOMNode(this.refs.title).value,
            writerName: ReactDOM.findDOMNode(this.refs.writerName).value,
            description: ReactDOM.findDOMNode(this.refs.description).value,
            categoryId: ReactDOM.findDOMNode(this.refs.categoryId).value
        });

        var category = _.findWhere(this.data.categories, {_id: ReactDOM.findDOMNode(this.refs.categoryId).value});
        if (!_.isUndefined(category)) {
            this.setState({
                category: {
                    _id: category._id,
                    slug: category.slug,
                    title: category.title
                }
            });
        }
    },

    handleTagsChange: function(data) {
        console.log(data);
        this.setState({
            tags: data
        });
    },

    handleDateChange: function(dateString, moment) {
        this.setState({
            releaseDate: dateString
        });
    },

    renderCategory() {
        return this.data.categories.map((category) => {
            return <option key={category._id} value={category._id}>{category.title}</option>
        });
    },

    updateArtist(result) {
        this.setState({
            artists: result
        });
    },

    updateRecordLabel(result) {
        this.setState({
            recordLabel: result
        });
    },

    render() {
        return <form className="ui form" onSubmit={this.handleSubmit}>
            <div className="field">
                <label>Album Name *</label>
                <input
                    name="title"
                    ref="title"
                    onChange={this.handleChange}
                    value={this.state.title}
                    placeholder="Album name here"/>
            </div>
            <div className="field">
                <label>Artist Name *</label>
                <SearchArtistInput
                    artists={this.state.artists}
                    handleChange={this.updateArtist} />
            </div>
            <div className="field">
                <label>Writer Name</label>
                <input
                    name="writerName"
                    ref="writerName"
                    onChange={this.handleChange}
                    value={this.state.writerName}
                    placeholder="Writer name here"/>
            </div>
            <div className="field">
                <label>Record Label *</label>
                <SearchRecordLabelInput
                    recordLabel={this.state.recordLabel}
                    handleChange={this.updateRecordLabel} />
            </div>
            <div className="field">
                <label>Album Description</label>
                <textarea
                    rows="2"
                    name="description"
                    ref="description"
                    onChange={this.handleChange}
                    value={this.state.description}
                    placeholder="Album description here"></textarea>
            </div>
            <div className="field">
                <label>Tags</label>
                <TagsInput 
                    ref="tags"
                    name="tags"
                    value={this.state.tags}
                    onChange={this.handleTagsChange} />
            </div>
            <div className="field">
                <label>What category is your music? *</label>
                <select
                    name="categoryId"
                    ref="categoryId"
                    className="ui fluid dropdown genre"
                    value={this.state.categoryId}
                    onChange={this.handleChange}>
                    <option>Select genre</option>
                    { this.renderCategory() }
                </select>
            </div>
            <div className="field">
                <label>Your Album Release Date *</label>
                <DatePicker
                    date={this.state.releaseDate}
                    onChange={this.handleDateChange}
                    hideFooter="true" />
            </div>
            <button className="ui right labeled icon blue button" type="submit">
                <i className="right arrow icon"></i>
                Next
            </button>
            <div className="ui error message"></div>
        </form>
    }
});
