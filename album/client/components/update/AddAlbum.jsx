AddAlbum = React.createClass({
    mixins: [ReactMeteorData],

    getMeteorData() {
        return {
    		album: {}
        };
    },

    render() {
        return  <div className="ui grid container main">
	        <div className="column">
	            <h2 className="ui top attached header">
	                <div className="content">
	                    Upload music
	                </div>
	            </h2>
	            <div className="ui attached segment">
	                <AlbumInfoForm album={this.data.album} />
	            </div>
	        </div>
	    </div>
    }
});
