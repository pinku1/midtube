SongItemUpdate = React.createClass({
    propTypes: {
        song: React.PropTypes.object.isRequired,
        album: React.PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            trackNumber: this.props.song.trackNumber || 1,
            title: this.props.song.title || '',
            description: this.props.song.description || '',
            writerName: this.props.song.writerName || '',
            artists: this.props.song.artists || []
        };
    },

    componentDidMount() {
        $(document).on('submit','form',function(e) {
            e.preventDefault();
        });

        $('.ui.form.song-update')
            .form({
                fields: {
                    trackNumber: {
                        identifier: 'trackNumber',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Track Number"'
                            }
                        ]
                    },
                    title: {
                        identifier: 'title',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter "Song Name"'
                            }
                        ]
                    }
                }
            });
    },

    handleChange(e) {
        this.setState({
            trackNumber: ReactDOM.findDOMNode(this.refs.trackNumber).value,
            title: ReactDOM.findDOMNode(this.refs.title).value,
            writerName: ReactDOM.findDOMNode(this.refs.writerName).value,
            description: ReactDOM.findDOMNode(this.refs.description).value
        });
    },

    handleBlur() {
        this.updateWithNewData({
            trackNumber: parseInt(ReactDOM.findDOMNode(this.refs.trackNumber).value.trim()),
            title: ReactDOM.findDOMNode(this.refs.title).value.trim(),
            writerName: ReactDOM.findDOMNode(this.refs.writerName).value.trim(),
            description: ReactDOM.findDOMNode(this.refs.description).value.trim(),
            artists: this.state.artists
        });
    },

    updateWithNewData(newData) {
        console.log(newData);

        Meteor.call('updateSong', this.props.song._id, newData);
    },

    closeUpdateModal() {
        this.handleBlur();
        //show modal box
        $('#song-' + this.props.song._id).modal('hide');
    },

    editSong(ev) {
        //show modal box
        $('#song-' + this.props.song._id)
            .modal({detachable: false, autofocus: true})
            .modal('show');
    },

    removeSong(ev) {
        Meteor.call('removeSong', this.props.song._id);
    },


    updateArtist(result) {
        this.setState({
            artists: result
        });
    },

    renderEditModal() {
        return <div className="ui modal fullscreen" id={'song-' + this.props.song._id}>
            <i className="close icon"></i>

            <div className="header">
                Update Song
            </div>
            <div className="content">
                <form className="ui form song-update">
                    <div className="field">
                        <label>Track Number *</label>
                        <input
                            type="number"
                            name="trackNumber"
                            ref="trackNumber"
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            value={this.state.trackNumber}
                            placeholder="Track number"/>
                    </div>
                    <div className="field">
                        <label>Song Name *</label>
                        <input
                            name="title"
                            ref="title"
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            value={this.state.title}
                            placeholder="Song name here"/>
                    </div>
                    <div className="field">
                        <label>Song Description</label>
                        <input
                            name="description"
                            ref="description"
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            value={this.state.description}
                            placeholder="Song description here"/>
                    </div>
                    <div className="field">
                        <label>Artist Name *</label>
                        <SearchArtistInput
                            artists={this.state.artists}
                            handleChange={this.updateArtist} />
                    </div>
                    <div className="field">
                        <label>Writer Name</label>
                        <input
                            name="writerName"
                            ref="writerName"
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            value={this.state.writerName}
                            placeholder="Song writer name here"/>
                    </div>
                    <div className="ui error message"></div>
                </form>
            </div>
            <div className="actions">
                <div className="ui labeled icon button" onClick={this.closeUpdateModal}>
                    <i className="right arrow icon"></i>
                    Update
                </div>
            </div>
        </div>
    },

    render() {
        return <tr>
            <td></td>
            <td>{s.pad(this.props.song.trackNumber, 2, '0')}</td>
            <td>{this.props.song.title}</td>
            <td><ArtistsNames artists={this.props.song.artists} /></td>
            <td>-</td>
            <td style={{'textAlign': 'right'}}>
                <div className="ui labeled icon mini button" onClick={this.editSong}>
                    <i className="edit icon"></i>
                    Edit
                </div>
                <div className="ui labeled icon mini button" onClick={this.removeSong}>
                    <i className="trash icon"></i>
                    Delete
                </div>
                {this.renderEditModal()}
            </td>
        </tr>
    }
});
