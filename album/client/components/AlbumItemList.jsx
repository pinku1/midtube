AlbumItemList = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    getAlbumUrl() {
        return FlowRouter.path('/album/:_id', {_id: this.props.album._id});
    },

    render() {
        return <div className="item">
            <a className="ui tiny rounded image" href={this.getAlbumUrl()}>
                <AlbumImage album={this.props.album} />
            </a>
            <div className="content">
                <a className="header" href={this.getAlbumUrl()}>
                    {truncate(titleize(this.props.album.title))}
                </a>
                <div className="description">
                    <p>
                        <ArtistsNames artists={this.props.album.artists} />
                    </p>
                    <span className="right floated">
                        <i className="thumbs up icon"></i>
                        {this.props.album.upVotes || 0}
                        &nbsp;&nbsp;
                        <i className="heart outline like icon"></i>
                        {this.props.album.favorites || 0}
                    </span>
                </div>
            </div>
        </div>
    }
});
