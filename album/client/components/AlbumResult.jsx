AlbumResultChild = React.createClass({
    propTypes: {
        albums: React.PropTypes.array.isRequired,
        totalCount: React.PropTypes.number.isRequired
    },

    getInitialState() {
        return {
            currentCount: AppSettings.limit,
            hasMore: false
        };
    },

    componentDidMount() {
        this.setState({
            hasMore: this.state.currentCount < this.props.totalCount
        });
    },

    componentWillReceiveProps(nextProps) {
        this.setState({
            hasMore: this.state.currentCount < nextProps.totalCount
        });
    },

    loadMore() {
        var increment = AppSettings.limit;
        var limit = increment * 2;
        if (!_.isUndefined(FlowRouter.getQueryParam("album-limit"))) {
            limit = parseInt(FlowRouter.getQueryParam("album-limit")) + increment;
        }
        
        this.setState({
            currentCount: limit
        });
        FlowRouter.setQueryParams({'album-limit': limit});
    },

    renderAlbums() {
        return this.props.albums.map((album) => {
            return <AlbumItem
                key={album._id}
                album={album} />
        });
    },

	render() {
		return <div style={{'width': '100%'}}>
			{this.props.totalCount > 0 ?
            <div className="ui doubling centered six column grid">
                {this.renderAlbums()}
            </div>
            : 
            <div className="ui message">
              <div className="header">
                No albums found.
              </div>
            </div>
            }
            <div className="ui grid">
                {this.state.hasMore ?
                <button className="ui button fluid" onClick={this.loadMore}>
                  Load more
                </button>
                : '' }
            </div>
		</div>
	}
});

AlbumResult = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData, SmoothScrollMixin],

    getMeteorData() {
        return {
            albums: Album.find({}, {sort: {createdAt: -1, baseScore: -1}}).fetch(),
            totalCount: Counts.get('total-albums-count')
        }
    },

    render() {
        return <AlbumResultChild
            albums={this.data.albums}
            totalCount={this.data.totalCount} />
    }
});
