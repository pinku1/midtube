AlbumImage = React.createClass({
    propTypes: {
        // This component gets the task to display through a React prop.
        // We can use propTypes to indicate it is required
        album: React.PropTypes.object.isRequired
    },

    getPicture() {
        if (!_.isNull(this.props.album.cover.path)) {
            return this.props.album.cover.path.replace(/ /g, '%20');
        } else {
            return '/images/album2.jpg';
        }
    },

    render() {
        var margin = '0 auto';
        if (this.props.float === 'left') {
            margin = '0';
        }
        return <img
            className="ui small bordered image album-img"
            src={this.getPicture()} title={this.props.album.title}
            style={{'width': '170px', 'height': 'auto', 'margin': margin}} />
    }
});
