FlowRouter.route('/albums', {
    name: 'albums',
    subscriptions: function(params, queryParams) {
        var albumLimit = parseInt(queryParams['album-limit']) || AppSettings.limit;
        this.register('findAlbums', Meteor.subscribe('findAlbums', null, albumLimit));
    },
    action: function() {
        ReactLayout.render(MainLayout, {
            content: <AlbumsList />
        });
    }
});

var albumRoutes = FlowRouter.group({
    prefix: '/album',
    name: 'album',
    triggersEnter: [function(context, redirect) {
        //console.log('running group triggers');
    }]
});

albumRoutes.route('/add', {
    name: 'albumAdd',
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <AddAlbum />
        });
    }
});

albumRoutes.route('/add/picture/:_id', {
    name: 'albumAddPicture',
    subscriptions: function(params) {
        this.register('singleAlbum', Meteor.subscribe('singleAlbum', params._id, true));
    },
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <AddAlbumPicture _id={params._id} />
        });
    }
});

albumRoutes.route('/update/info/:_id', {
    name: 'albumUpdate',
    subscriptions: function(params) {
        this.register('singleAlbum', Meteor.subscribe('singleAlbum', params._id, true));
    },
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <UpdateAlbum _id={params._id} />
        });
    }
});

albumRoutes.route('/update/songs/:_id', {
    name: 'albumSongsUpdate',
    subscriptions: function(params) {
        this.register('singleAlbum', Meteor.subscribe('singleAlbum', params._id, true));
    },
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <UpdateAlbumSongs _id={params._id} />
        });
    }
});

albumRoutes.route('/update/links/:_id', {
    name: 'albumLinksUpdate',
    subscriptions: function(params) {
        this.register('singleAlbum', Meteor.subscribe('singleAlbum', params._id, true));
    },
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <UpdateAlbumLinks _id={params._id} />
        });
    }
});

albumRoutes.route('/update/extras/:_id', {
    name: 'albumExtrasUpdate',
    subscriptions: function(params) {
        this.register('singleAlbum', Meteor.subscribe('singleAlbum', params._id, true));
    },
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <UpdateAlbumExtras _id={params._id} />
        });
    }
});

albumRoutes.route('/:_id', {
    name: 'albumView',
    subscriptions: function(params) {
        this.register('singleAlbum', Meteor.subscribe('singleAlbum', params._id));
    },
    action: function(params) {
        ReactLayout.render(MainLayout, {
            content: <AlbumView _id={params._id} />
        });
    }
});
