var requiredString = Validators.and([
    Validators.required(),
    Validators.string(),
    Validators.minLength(3),
    Validators.maxLength(40)    
]);
var requiredDate = Validators.and([
    Validators.required(),
    Validators.date() 
]);

Albums = new Mongo.Collection('albums');

AlbumArtist = Astro.Class({
    name: 'AlbumArtist',
    fields: {
        _id: {
            type: 'string'
        },
        slug: {
            type: 'string'
        },
        fullName: {
            type: 'string'
        }
    }
});

Album = Astro.Class({
    name: 'Album',
    collection: Albums,
    fields: {
        title: {
            type: 'string',
            validator: requiredString
        },
        slug: 'string',
        description: 'string',
        featured: {
            type: 'boolean',
            default: false
        },
        ownerId: 'string',
        ownerArtistId: 'string',
        artists: {
            type: 'array',
            nested: 'AlbumArtist',
            default: function() {
                return [];
            }
        },
        writerName: 'string',
        recordLabel: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'recordLabel',
                fields: {
                    _id: {
                        type: 'string'
                    },
                    slug: {
                        type: 'string'
                    },
                    title: {
                        type: 'string'
                    }
                }
            }
        },
        releaseDate: {
            type: 'date',
            validator: requiredDate
        },
        links: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'Links',
                fields: {
                    itunes: 'string',
                    googlePlay: 'string',
                    soundCloud: 'string',
                    amazon: 'string'
                }
            }
        },
        cover: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'Cover',
                fields: {
                    name: 'string',
                    pathOriginal: 'string',
                    path: 'string',
                    size: {
                        type: 'number',
                        validator: Validators.number(),
                        default: 0
                    },
                    type: 'string'
                }
            }
        },
        sales: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'Sales',
                fields: {
                    isFree: {
                        type: 'boolean',
                        validator: Validators.boolean(),
                        default: true
                    },
                    price: {
                        type: 'number',
                        validator: Validators.number(),
                        default: 0
                    },
                    songPrice: {
                        type: 'number',
                        validator: Validators.number(),
                        default: 0
                    }
                }
            }
        },
        baseScore: {
            type: 'number',
            default: 0
        },
        upVotes: {
            type: 'number',
            default: 0
        },
        upVoters: {
            type: 'array',
            default: function() {
                return [];
            }
        },
        favorites: {
            type: 'number',
            default: 0
        },
        favoriteBy: {
            type: 'array',
            default: function() {
                return [];
            }
        },
        isActive: {
            type: 'boolean',
            default: true
        },
        tags: {
            type: 'array',
            default: function() {
                return [];
            }
        },
        category: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'category',
                fields: {
                    _id: {
                        type: 'string'
                    },
                    slug: {
                        type: 'string'
                    },
                    title: {
                        type: 'string'
                    }
                }
            }
        },
        isProcessed: {
            type: 'boolean',
            default: false
        },
        file: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'Files',
                fields: {
                    zip: 'string',
                    zipSource: 'string',
                    meta: 'object'
                }
            }
        }
    },
    behaviors: {
        slug: {
            fieldName: 'title',
            methodName: null,
            slugFieldName: 'slug',
            canUpdate: true,
            unique: true,
            separator: '-'
        },
        timestamp: {
            hasCreatedField: true,
            createdFieldName: 'createdAt',
            hasUpdatedField: true,
            updatedFieldName: 'updatedAt'
        }
    },
    events: {
        'afterInsert': function (e) {
            if (Meteor.isServer) {
                //addAlgoliaIndex([{
                //    objectID: e.target._id,
                //    title: e.target.title,
                //    score: 10
                //}]);
            }
        }
    }
});
