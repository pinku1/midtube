var requiredString = Validators.and([
    Validators.required(),
    Validators.string(),
    Validators.minLength(3),
    Validators.maxLength(40)
]);
var requiredDate = Validators.and([
    Validators.required(),
    Validators.date()
]);

Songs = new Mongo.Collection('songs');

SongArtist = Astro.Class({
    name: 'SongArtist',
    fields: {
        _id: {
            type: 'string'
        },
        slug: {
            type: 'string'
        },
        fullName: {
            type: 'string'
        }
    }
});

Song = Astro.Class({
    name: 'Song',
    collection: Songs,
    fields: {
        trackNumber: {
            type: 'number',
            validator: Validators.number(),
            default: 0
        },
        title: {
            type: 'string',
            validator: requiredString
        },
        slug: {
            type: 'string',
            validator: Validators.unique()
        },
        description: 'string',
        featured: {
            type: 'boolean',
            default: false
        },
        file: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'file',
                fields: {
                    name: 'string',
                    path: 'string',
                    pathSource: 'string',
                    size: {
                        type: 'number',
                        validator: Validators.number(),
                        default: 0
                    },
                    type: 'string',
                    meta: 'object'
                }
            }
        },
        fileStream: {
            type: 'object',
            default: function() {
                return {};
            },
            nested: {
                name: 'fileStream',
                fields: {
                    name: 'string',
                    path: 'string',
                    pathSource: 'string',
                    size: {
                        type: 'number',
                        validator: Validators.number(),
                        default: 0
                    },
                    type: 'string',
                    meta: 'object'
                }
            }
        },
        ownerId: 'string',
        albumId: 'string',
        artists: {
            type: 'array',
            nested: 'SongArtist',
            default: function() {
                return [];
            }
        },
        writerName: 'string',
        playCount: {
            type: 'number',
            validator: Validators.number(),
            default: 0
        },
        isActive: {
            type: 'boolean',
            default: true
        },
        tags: {
            type: 'array',
            default: function() {
                return [];
            }
        },
        isProcessed: {
            type: 'boolean',
            default: false
        }
    },
    behaviors: {
        slug: {
            fieldName: 'title',
            methodName: null,
            slugFieldName: 'slug',
            canUpdate: true,
            unique: true,
            separator: '-'
        },
        timestamp: {
            hasCreatedField: true,
            createdFieldName: 'createdAt',
            hasUpdatedField: true,
            updatedFieldName: 'updatedAt'
        }
    }
});
