var markAlbumNotProcessed = function (albumId) {
    var album = Album.findOne({_id: albumId});
    album.set({isProcessed: false});

    album.validate();

    if (album.hasValidationErrors()) {
        album.throwValidationException();
    } else {
        album.save();
        return album;
    }
};

Meteor.methods({
    'createAlbum': function (doc) {
        if (_.isNull(this.userId)) {
            //if not logged in
            throw new Meteor.Error('not-logged-in', 'Please login to continue.');
        }

        var artist = Artist.findOne({userId: this.userId});

        //make sure owner is current logged in user
        doc = _.extend(doc, {
            ownerId: this.userId,
            ownerArtistId: artist._id
        });

        var album = new Album();
        album.set(doc);

        album.validate();

        if (album.hasValidationErrors()) {
            album.throwValidationException();
        } else {
            album.save();
            return album;
        }
    },

    'updateAlbum': function (docId, modifier) {
        if (_.isNull(this.userId)) {
            //if not logged in
            throw new Meteor.Error('not-logged-in', 'Please login to continue.');
        }

        //make sure owner is current logged in user
        var album = Album.findOne({_id: docId, ownerId: this.userId});

        //clean and trim
        if (!_.isUndefined(modifier.title)) {
            modifier.title = clean(modifier.title);
        }
        if (!_.isUndefined(modifier.writerName)) {
            modifier.writerName = clean(modifier.writerName);
        }
        if (!_.isUndefined(modifier.description)) {
            modifier.description = s.stripTags(clean(modifier.description));
        }

        album.set(modifier);

        album.validate();

        if (album.hasValidationErrors()) {
            album.throwValidationException();
        } else {
            album.save();
            return album;
        }
    },

    'removeAlbum': function(docId) {
        if (_.isNull(this.userId)) {
            //if not logged in
            throw new Meteor.Error('not-logged-in', 'Please login to continue.');
        }

        //todo remove songs + files also

        //check if update is owner of album
        return Album.remove({_id: docId, ownerId: this.userId});
    },

    'addSong': function (doc) {
        if (_.isNull(this.userId)) {
            //if not logged in
            throw new Meteor.Error('not-logged-in', 'Please login to continue.');
        }

        //make sure owner is current logged in user
        doc = _.extend(doc, {
            ownerId: this.userId
        });

        var song = new Song();
        song.set(doc);

        song.save();

        //mark album not processed
        Meteor.call('markAlbumNotProcessed', song.albumId);

        return song;
    },

    'updateSong': function(docId, modifier) {
        if (_.isNull(this.userId)) {
            //if not logged in
            throw new Meteor.Error('not-logged-in', 'Please login to continue.');
        }

        //make sure owner is current logged in user
        var song = Song.findOne({_id: docId});
        song.set(modifier);

        song.validate();

        if (song.hasValidationErrors()) {
            song.throwValidationException();
        } else {
            song.save();
            return song;
        }
    },

    'removeSong': function(docId) {
        if (_.isNull(this.userId)) {
            //if not logged in
            throw new Meteor.Error('not-logged-in', 'Please login to continue.');
        }

        //check if update is owner of album
        //secrity check only owner can edit
        //todo delete file from amazon s3

        //mark album not processed
        var song = Song.findOne({_id: docId});
        Meteor.call('markAlbumNotProcessed', song.albumId);

        return Song.remove({_id: docId, ownerId: this.userId});
    },

    'updateSongCounter': function(docId) {
        return Song.update({_id: docId}, {$inc: {playCount: 1}});
    },

    'markAlbumNotProcessed': function (albumId) {
        console.log(albumId);
        var album = Album.findOne({_id: albumId});
        album.set({isProcessed: false});

        album.validate();

        if (album.hasValidationErrors()) {
            album.throwValidationException();
        } else {
            album.save();
            return album;
        }
    }
});
