var requiredString = Validators.and([
    Validators.required(),
    Validators.string(),
    Validators.minLength(3),
    Validators.maxLength(40)
]);

Categories = new Mongo.Collection('categories');

Category = Astro.Class({
    name: 'Category',
    collection: Categories,
    fields: {
        title: {
            type: 'string',
            validator: requiredString
        },
        slug: 'string',
        description: 'string',
        tags: {
            type: 'array',
            default: function() {
                return [];
            }
        },
        isActive: {
            type: 'boolean',
            default: true
        }
    },
    behaviors: {
        slug: {
            fieldName: 'title',
            methodName: null,
            slugFieldName: 'slug',
            canUpdate: true,
            unique: true,
            separator: '-'
        },
        timestamp: {
            hasCreatedField: true,
            createdFieldName: 'createdBy',
            hasUpdatedField: true,
            updatedFieldName: 'updatedBy'
        }
    }
});
