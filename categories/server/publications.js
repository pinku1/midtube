Meteor.publish('allCategories', function() {
    return Category.find({isActive: true}, {sort: {title: 1}});
});
